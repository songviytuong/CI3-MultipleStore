<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        $data['page_class'] = "home-page";
        $data['js_to_load'] = array();

//        if (!empty($products)) {
//            SEOPlugin::setTitle($products['Title']);
//            SEOPlugin::setDescription(($products['Description']) ? $products['Description'] : $products['MetaDescription']);
//            SEOPlugin::setSocialImage($products['PrimaryImage']);
//        }

        $this->template->write_view('content', __CLASS__, $data);
        $this->template->render();
    }

}
