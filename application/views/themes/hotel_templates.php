<?php
$session_id = $this->session->session_id;
echo doctype('html5');
?>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" /> 
        <?php echo SEOPlugin::getBaseUrl(); ?>

        <title><?php echo SEOPlugin::getTitle(); ?></title><?php if (SEOPlugin::getKeywords()) { ?>
            <meta name="keywords" content="<?php echo SEOPlugin::getKeywords(); ?>" /><?php } ?>   
        <meta name="description" content="<?php echo SEOPlugin::getDescription(); ?>" />
        <meta property='og:title' content="<?php echo SEOPlugin::getTitle(); ?>" />
        <meta property='og:description' content="<?php echo SEOPlugin::getDescription(); ?>" />
        <?php echo SEOPlugin::getSocialImage(); ?>

        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo THEMES_URL; ?>/images/fav/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo THEMES_URL; ?>/images/fav/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo THEMES_URL; ?>/images/fav/favicon-16x16.png" />

        <link rel="mask-icon" href="<?php echo THEMES_URL; ?>/images/fav/safari-pinned-tab.svg" color="#ff0000" />

        <link href='<?php echo THEMES_URL; ?>/css/hotel.css?v=1.004' rel='stylesheet' type='text/css' media='all'>
        <link href='<?php echo THEMES_URL; ?>/css/bootstrap-new-3-2.css?v=1.004' rel='stylesheet' type='text/css' media='all'>
        <link href='<?php echo THEMES_URL; ?>/css/header-footer-css.css?v=1.004' rel='stylesheet' type='text/css' media='all'>
        <link rel="shortcut icon" href="<?php echo THEMES_URL; ?>/images/favicon.ico">
        <script src='<?php echo THEMES_URL; ?>/js/jquery-1.7.2.min.js' type='text/javascript'></script>
        <script src='<?php echo THEMES_URL; ?>/js/custom.js' type='text/javascript'></script>
        <script src='<?php echo THEMES_URL; ?>/js/jquery.langdd.js' type='text/javascript'></script>
        <script src='<?php echo THEMES_URL; ?>/js/common_func.js' type='text/javascript'></script>
        <script src='<?php echo THEMES_URL; ?>/js/bootstrap-new-3-2.js' type='text/javascript'></script>
        
        <script type="text/javascript" src="<?php echo THEMES_URL; ?>/js/jquery.lazyload.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo THEMES_URL; ?>/popups/shadowbox.css" />

        <link rel="stylesheet" type="text/css" href="<?php echo THEMES_URL; ?>/css/msdropdown/langdd.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo THEMES_URL; ?>/css/msdropdown/flags.css" />

        <script type="text/javascript" src="<?php echo THEMES_URL; ?>/popups/shadowbox.js"></script>

        <script>Shadowbox.init({handleOversize: "drag", modal: true});
            $(document).ready(function () {
                $(".ask").live("click", function () {
                    $tab = "";
                    if ($(this).attr("tab")) {
                        $tab = "#" + $(this).attr("tab");
                    } else if ($(this).attr("id")) {
                        $tab = "#" + $(this).attr("id");
                    }

                    Shadowbox.open({content: 'https://www.indianholiday.com' + '/ask-expert.php' + $tab, player: "iframe", height: 700, width: 810});
                    return false;
                });
            });</script>


        <script type="application/ld+json">
            {
            "@context": "https://schema.org",
            "@type": "Organization",
            "name": "Indian Holiday Pvt Ltd",
            "url": "https://www.indianholiday.com",
            "logo": "/images/logo.png",
            "contactPoint": [
            {	"@type": "ContactPoint",
            "telephone": "+91-11-424-23100",
            "contactType": "customer service"
            }
            ],
            "sameAs": [
            "https://twitter.com/indianholiday",
            "https://plus.google.com/+IndianHolidaypublicLimitedNewDelhi",
            "https://www.facebook.com/IndianHolidayPvtLtd",
            "https://www.linkedin.com/company/indian-holiday-pvt-ltd",
            "https://www.youtube.com/c/indianholidaypubliclimitednewdelhi",
            "https://www.pinterest.com/indianholiday/"
            ]
            }
        </script>

        <script type="text/javascript">
            function loadMore(start, load, parentId, childClass, loadMoreBtn) {
                $('#' + parentId + ' div.' + childClass).css('display', 'none');
                var divStr = '#' + parentId + ' div.' + childClass;
                var size_li = $(divStr).size();
                var x = start;
                var loadList = load;
                $(divStr + ':lt(' + x + ')').show();
                $('#' + loadMoreBtn).click(function () {
                    x = (x + loadList <= size_li) ? x + loadList : size_li;
                    $(divStr + ':lt(' + x + ')').slideDown();
                    if (x == size_li) {
                        $('#' + loadMoreBtn).hide();
                    }
                });
            }
        </script>

        <?php
        if (ENVIRONMENT !== 'development') {
            if (_ANALYTICS && _ANALYTICS_ID != '') {
                ?>	
                <script>
                    (function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                                m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, "script", "//www.google-analytics.com/analytics.js", "ga");
                    ga("create", "<?php echo _ANALYTICS_ID; ?>", "auto");
                    ga("send", "pageview");
                </script>';
                <?php
            }
        }
        ?>
    </head>
    <body class="<?php echo!empty($page_class) ? $page_class : ''; ?>">
        <script>
            $(function () {
                $("img.lazy").lazyload();
            });
            jQuery(document).ready(function ($) {

                $('body').addClass('js');
                var $menu = $('#new-menu'),
                        $menulink = $('.new-menu-link'),
                        $menuTrigger = $('.has-submenu > .clickmu');

                $menulink.click(function (e) {
                    e.preventDefault();
                    $menulink.toggleClass('active');
                    $menu.toggleClass('active');
                });

                $menuTrigger.click(function (e) {
                    e.preventDefault();
                    var $this = $(this);
                    var classname = $this.attr('class');
                    $menuTrigger.removeClass('active').next('ul').removeClass('active');
                    if (classname != 'clickmu active') {
                        $this.addClass('active').next('ul').addClass('active');
                    }
                });
            });

        </script>

        <script>
            $(document).ready(function () {
                $('.call-optbx').click(function (e) {
                    e.preventDefault();
                    $(this).toggleClass('active');
                    $('.call-dropdown').slideToggle();
                });
                $('.call-dropdown .closebtt').click(function (e) {
                    e.preventDefault();
                    $('.call-optbx').removeClass('active');
                    $('.call-dropdown').slideUp();
                });
            });
            function closeCallBox() {
                $('.call-optbx').removeClass('active');
                $('.call-dropdown').slideUp();
            }

        </script>

        <!--sphider_noindex-->
        <!--header start -->
        <script>
            $title = "General Query";
            $(document).ready(function (e) {
                $title = $("h1").eq(0).text();
            });
            if ($title == "")
                $title = "General Query";
        </script>

        <?php
        if (!empty($header)) {
            echo $header;
        }
        if (!empty($content)) {
            echo $content;
        }
        if (!empty($footer)) {
            echo $footer;
        }
        ?>

        <?php
        if (!empty($js_to_load) && is_array($js_to_load)) {
            foreach ($js_to_load as $row) {
                echo '<script src="' . $row . '" type="text/javascript" ></script>';
            }
        }
        ?>


        <?php
        if (_COMMENTOFF && _COMMENT == 'FB') {
            ?>
            <div id="fb-root"></div>
            <script>(function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id))
                        return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <?php
        }
        if (ENVIRONMENT === 'development') {
            ?>
            <p style="float:right;padding:20px">Total <?php echo @count($this->db->queries); ?> queries .Page rendered in <strong>{elapsed_time}</strong> seconds. Memory Usage <strong><?php echo round(memory_get_usage() / 1024); ?> </strong>Kb</p>
            <?php
            echo '<div class="clearfix"></div><div style="margin-left:250px;float:right">';
            if (@count($this->db->queries) > 0) {
                foreach ($this->db->queries as $query) {
                    print_r($query);
                    echo '<hr>';
                }
            }
            echo '</div>';
        }
        ?>
        <?php
        if (ENVIRONMENT == 'production' || ENVIRONMENT == 'staging') {
            if (time() % 2 == 0) {
                ?>
                <script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=93844322"></script>
                <?php
            } else {
                ?>
                <script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=24111962"></script>
                <?php
            }
        }
        ?>



        <script>
            function createByJson() {
                var jsonData = [];
                $("#byjson").msDropDown({byJson: {data: jsonData, name: 'payments2'}}).data("dd");
            }
            $(document).ready(function (e) {		  //$.noConflict();	
                //no use 
                //var jQuery132 = $.noConflict(true);
                try {
                    var pages = $("#pages").msDropdown({on: {change: function (data, ui) {
                                var val = data.value;
                                if (val != "")
                                    window.location = val;
                            }}}).data("dd");

                    var pagename = document.location.pathname.toString();
                    pagename = pagename.split("/");
                    pages.setIndexByValue(pagename[pagename.length - 1]);
                    $("#ver").html(msBeautify.version.msDropdown);
                } catch (e) {
                    //console.log(e);	
                }

                $("#ver").html(msBeautify.version.msDropdown);

                //convert
                $("#countries_dd").msDropdown({roundedBorder: false});
                createByJson();
                $("#tech").data("dd");


                $('#countries_dd').on('change', function () {
                    var url = $(this).val(); // get selected value

                    //alert($(this).attr('data-title').val());

                    var selected = $(this).find('option:selected');

                    if (url) { // require a URL
                        if (url == 'RU')
                        {
                            window.open('http' + '://www.' + 'indianholiday' + '.ru/', '_blank');
                        }
                        else
                        {
                            window.location = url; // redirect
                        }
                    }
                    return false;
                });
            });


            $(document).ready(function (e) {		  //$.noConflict();	
                //no use 
                //var jQuery132 = $.noConflict(true);
                try {
                    var pages = $("#pages").msDropdown({on: {change: function (data, ui) {
                                var val = data.value;
                                if (val != "")
                                    window.location = val;
                            }}}).data("dd");

                    var pagename = document.location.pathname.toString();
                    pagename = pagename.split("/");
                    pages.setIndexByValue(pagename[pagename.length - 1]);
                    $("#ver").html(msBeautify.version.msDropdown);
                } catch (e) {
                    //console.log(e);	
                }

                $("#ver").html(msBeautify.version.msDropdown);

                //convert
                $("#countries_dd1").msDropdown({roundedBorder: false});
                createByJson();
                $("#tech").data("dd");


                $('#countries_dd1').on('change', function () {
                    var url = $(this).val(); // get selected value

                    //alert($(this).attr('data-title').val());

                    var selected = $(this).find('option:selected');

                    if (url) { // require a URL
                        if (url == 'RU')
                        {
                            window.open('http' + '://www.' + 'indianholiday' + '.ru/', '_blank');
                        }
                        else
                        {
                            window.location = url; // redirect
                        }
                    }
                    return false;
                });
            });

            function showValue(h) {
                console.log(h.name, h.value);
            }
            $("#tech").change(function () {
                console.log("by jquery: ", this.value);
            })

            //
        </script>
        <script type="text/javascript">

            $(function () {
                $('div.footerlinkview').hide();
                $('a.read112').click(function () {
                    $(this).parent().parent().find("div.footerlinkview").slideToggle(400);
                    $(this).text($(this).text() == '- Less' ? '+ More' : '- Less');
                    return false;
                });
            });

        </script>
        <script src='<?php echo THEMES_URL; ?>/js/jquery.tabslideout.v1.3.js' type='text/javascript'></script>
        <script src='<?php echo THEMES_URL; ?>/js/auto-suggestion.js' type='text/javascript'></script>
        <script type="text/javascript">
            $(document).ready(function () {
            });
        </script>
    </body>
</html>