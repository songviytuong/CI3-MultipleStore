<?php
$session_id = $this->session->session_id;
echo doctype('html5');
?>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <?php echo SEOPlugin::getBaseUrl(); ?>
        
        <title><?php echo SEOPlugin::getTitle(); ?></title><?php if (SEOPlugin::getKeywords()) { ?>
            <meta name="keywords" content="<?php echo SEOPlugin::getKeywords(); ?>" /><?php } ?>   
        <meta name="description" content="<?php echo SEOPlugin::getDescription(); ?>" />
        <meta property='og:title' content="<?php echo SEOPlugin::getTitle(); ?>" />
        <meta property='og:description' content="<?php echo SEOPlugin::getDescription(); ?>" />
        <?php echo SEOPlugin::getSocialImage(); ?>
        
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo THEMES_URL; ?>/images/fav/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo THEMES_URL; ?>/images/fav/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo THEMES_URL; ?>/images/fav/favicon-16x16.png" />
        <link rel="manifest" href="<?php echo THEMES_URL; ?>/images/fav/manifest.json" />
        <link rel="mask-icon" href="<?php echo THEMES_URL; ?>/images/fav/safari-pinned-tab.svg" color="#ff0000" />

        <?php
        if (ENVIRONMENT !== 'development') {
            if (_ANALYTICS && _ANALYTICS_ID != '') {
                ?>	
                <script>
                    (function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                                m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, "script", "//www.google-analytics.com/analytics.js", "ga");
                    ga("create", "<?php echo _ANALYTICS_ID; ?>", "auto");
                    ga("send", "pageview");
                </script>';
                <?php
            }
        }
        ?>
    </head>
    <body class="<?php echo!empty($page_class) ? $page_class : ''; ?>">
        <?php
        if (!empty($header)) {
            echo $header;
        }
        if (!empty($content)) {
            echo $content;
        }
        if (!empty($footer)) {
            echo $footer;
        }
        ?>

        <?php
        if (!empty($js_to_load) && is_array($js_to_load)) {
            foreach ($js_to_load as $row) {
                echo '<script src="' . $row . '" type="text/javascript" ></script>';
            }
        }
        ?>


        <?php
        if (_COMMENTOFF && _COMMENT == 'FB') {
            ?>
            <div id="fb-root"></div>
            <script>(function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id))
                        return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <?php
        }
        if (ENVIRONMENT === 'development') {
            ?>
            <p style="float:right;padding:20px">Total <?php echo @count($this->db->queries); ?> queries .Page rendered in <strong>{elapsed_time}</strong> seconds. Memory Usage <strong><?php echo round(memory_get_usage() / 1024); ?> </strong>Kb</p>
            <?php
            echo '<div class="clearfix"></div><div style="margin-left:250px;float:right">';
            if (@count($this->db->queries) > 0) {
                foreach ($this->db->queries as $query) {
                    print_r($query);
                    echo '<hr>';
                }
            }
            echo '</div>';
        }
        ?>
        <?php
        if (ENVIRONMENT == 'production' || ENVIRONMENT == 'staging') {
            if (time() % 2 == 0) {
                ?>
                <script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=93844322"></script>
                <?php
            } else {
                ?>
                <script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=24111962"></script>
                <?php
            }
        }
        ?>
    </body>
</html>