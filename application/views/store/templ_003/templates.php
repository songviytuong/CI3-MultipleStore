<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
    <meta name="description" content="Customize and book your tailor-made holiday tours to Vietnam, Laos, Cambodia, Myanmar, Thailand with TNK Travel." />
    <meta name="keywords" content="Vietnam tours, Vietnam holidays, Vietnam travel, Vietnam vacations, Laos tours, Cambodia tours, Myanmar tours, Thailand tours" />
    <link rel='stylesheet' id='page-list-style-css' href='<?= $TEMPLATE_SRC; ?>/css/page-list.css?ver=4.3' type='text/css' media='all' />
    <link rel='stylesheet' id='dosis-fonts-css' href='https://fonts.googleapis.com/css?family=Dosis%3A400%2C600&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
    <link rel='stylesheet' id='style-package-css' href='<?= $TEMPLATE_SRC; ?>/css/style.package.min.css?ver=1.0' type='text/css' media='all' />
    <link rel='stylesheet' id='responsive-css' href='<?= $TEMPLATE_SRC; ?>/css/responsive.css?ver=4.1' type='text/css' media='all' />
    <script type='text/javascript' src='<?= $TEMPLATE_SRC; ?>/js/jquery/jquery.js'></script>
    <script type='text/javascript' src='<?= $TEMPLATE_SRC; ?>/js/jquery/jquery-migrate.min.js'></script>
<style type="text/css">
    .recentcomments a {
        display: inline !important;
        padding: 0 !important;
        margin: 0 !important
    }
</style>
<style type="text/css">
    .broken_link,
    a.broken_link {
        text-decoration: line-through
    }
</style>
</head>

<body class="home blog">
    <div class="ct-preloader" style="display:none; z-index:-9999">
        <div class="ct-preloader-content"></div>
    </div>
    <div id="page-wrapper">
        <header id="header"><div class="topnav"><div class="container"><ul class="quick-menu"><li class="no-margin"><i class="fa fa-phone"></i>PHONE: (+84 28) 3920 4766 (16 lines)</li><li><i class="fa fa-envelope-o"></i>EMAIL: <a href="mailto:info@tnktravel.com" rel="noopener">info@tnktravel.com</a></li></ul><ul id="menu-menu-top-header" class="quick-menu pull-left"><li id="menu-item-15895" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15895"><a href="https://www.tnktravel.com/about-us/">About us</a></li><li id="menu-item-15896" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15896"><a href="https://www.tnktravel.com/terms-conditions/">Terms &amp; Conditions</a></li><li id="menu-item-15897" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15897"><a href="https://www.tnktravel.com/our-licences/">Our licences</a></li><li id="menu-item-1581" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1581"><a href="https://www.tnktravel.com/frequently-asked-questions/">F.A.Q</a></li><li id="menu-item-9322" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-9322"><a href="https://www.tnktravel.com/blogs/">Travel Blogs</a></li><li id="menu-item-1566" class="btn btn-xs menu-item menu-item-type-post_type menu-item-object-page menu-item-1566"><a href="https://www.tnktravel.com/contact-us/">Contact us</a></li></ul><ul class="quick-menu pull-right dropit" id="quickmenu"><li class="dropit-trigger"><i class="fa fa-language"></i>
<a href="#">English
<i class="fa fa-angle-down"></i>
<i class="fa fa-angle-up"></i></a><ul style="display: none;" class="dropit-submenu"><li><a href="https://www.tnktravel.com" title="English" rel="noopener">English</a></li><li><a href="http://www.trippy.vn/" target="_blank" title="Vietnamese" rel="noopener">Vietnamese</a></li></ul></li></ul></div></div><div id="main-header" class=""><div class="main-header header-section-4" data-sticky="1" id="header-section"><div class="container"><div class="row pt20" id="middle-header"><div class="col-md-3"><div class="logo navbar-brand full-width">
<a href="https://www.tnktravel.com">
<img src="https://www.tnktravel.com/wp-content/themes/default_v4/images/logo.png" alt="TNKTRAVEL">
</a></div></div><div class="col-md-9">
<a href="https://www.tnktravel.com/tours/vietnam-tours/vietnam-tour-packages/">
<img src="https://www.tnktravel.com/wp-content/themes/default_v4/data/banner-header-01.jpg" alt="">
</a></div></div><div class="row">
<nav class="navi main-nav" id="main-nav"><ul id="main-nav" class=""><li id="menu-item-1496" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1496"><a href="https://www.tnktravel.com/">Home</a></li><li id="menu-item-1528" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-1528 has-child"><a href="https://www.tnktravel.com/tours/vietnam-tours/">Vietnam Tours</a><ul class="sub-menu"><li id="menu-item-5135" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5135"><a href="https://www.tnktravel.com/tours/vietnam-tours/mekong-delta-tours/">Mekong Delta Tours</a></li><li id="menu-item-5136" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5136"><a href="https://www.tnktravel.com/tours/vietnam-tours/mekong-delta-cruises/">Mekong Delta Cruises</a></li><li id="menu-item-1536" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1536"><a href="https://www.tnktravel.com/tours/vietnam-tours/vietnam-tour-packages/">Vietnam Tour Packages</a></li><li id="menu-item-6458" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6458"><a href="https://www.tnktravel.com/tours/vietnam-tours/vietnam-muslim-tours/">Vietnam Muslim Tours</a></li><li id="menu-item-1532" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1532"><a href="https://www.tnktravel.com/tours/vietnam-tours/vietnam-adventure-tours/">Vietnam Adventure Tours</a></li><li id="menu-item-5134" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5134"><a href="https://www.tnktravel.com/tours/vietnam-tours/halong-bay-cruises/">Halong Bay Cruises</a></li><li id="menu-item-5137" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5137"><a href="https://www.tnktravel.com/tours/vietnam-tours/trekking-kayaking-and-cycling/">Trekking Kayaking and Cycling</a></li><li id="menu-item-5138" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5138"><a href="https://www.tnktravel.com/tours/vietnam-tours/vietnam-beach-breaks/">Vietnam Beach Breaks</a></li><li id="menu-item-5139" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5139"><a href="https://www.tnktravel.com/tours/vietnam-tours/vietnam-day-trips/">Vietnam Day Trips</a></li><li id="menu-item-10698" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-10698"><a href="https://www.tnktravel.com/tours/vietnam-tours/saigon-day-trips/">Saigon Day Trips</a></li></ul></li><li id="menu-item-5140" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5140"><a href="https://www.tnktravel.com/tours/laos-tours/">Laos Tours</a></li><li id="menu-item-5146" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5146"><a href="https://www.tnktravel.com/tours/cambodia-tours/">Cambodia Tours</a></li><li id="menu-item-5155" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5155"><a href="https://www.tnktravel.com/tours/myanmar-tours/">Myanmar Tours</a></li><li id="menu-item-5161" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5161"><a href="https://www.tnktravel.com/tours/thailand-tours/">Thailand Tours</a></li><li id="menu-item-5162" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5162"><a href="https://www.tnktravel.com/tours/multi-country-tours/">Multi Country Tours</a></li><li id="menu-item-15819" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15819"><a href="https://www.tnktravel.com/customized-tours/">Customized Tours</a></li><li id="menu-item-5163" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-5163 has-child"><a href="https://www.tnktravel.com/hotels/">Hotels</a><ul class="sub-menu"><li id="menu-item-15818" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-15818 has-child"><a href="https://www.tnktravel.com/hotels/hotels-in-vietnam/">Hotels in Vietnam</a><ul class="sub-menu"><li id="menu-item-15820" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-15820"><a href="https://www.tnktravel.com/hotels/hotels-in-vietnam/hotels-in-ho-chi-minh/">Hotels in Ho Chi Minh city</a></li></ul></li><li id="menu-item-15817" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-15817"><a href="https://www.tnktravel.com/hotels/hotels-in-laos/">Hotels in Laos</a></li><li id="menu-item-5164" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5164"><a href="https://www.tnktravel.com/hotels/hotels-in-cambodia/">Hotels in Cambodia</a></li><li id="menu-item-5167" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5167"><a href="https://www.tnktravel.com/hotels/hotels-in-myanmar/">Hotels in Myanmar</a></li><li id="menu-item-5169" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5169"><a href="https://www.tnktravel.com/hotels/hotels-in-thai-lan/">Hotels in Thailand</a></li></ul></li></ul>                        </nav></div></div></div><div class="header-mobile houzez-header-mobile" data-sticky="0"><div class="container"><div class="mobile-nav">
<span class="nav-trigger"><i class="fa fa-navicon"></i></span><div class="nav-dropdown main-nav-dropdown"><ul id="main-nav" class=""><li id="menu-item-1496" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1496"><a href="https://www.tnktravel.com/">Home</a></li><li id="menu-item-1528" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-1528 has-child"><a href="https://www.tnktravel.com/tours/vietnam-tours/">Vietnam Tours</a><ul class="sub-menu" style="display: none;"><li id="menu-item-5135" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5135"><a href="https://www.tnktravel.com/tours/vietnam-tours/mekong-delta-tours/">Mekong Delta Tours</a></li><li id="menu-item-5136" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5136"><a href="https://www.tnktravel.com/tours/vietnam-tours/mekong-delta-cruises/">Mekong Delta Cruises</a></li><li id="menu-item-1536" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1536"><a href="https://www.tnktravel.com/tours/vietnam-tours/vietnam-tour-packages/">Vietnam Tour Packages</a></li><li id="menu-item-6458" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6458"><a href="https://www.tnktravel.com/tours/vietnam-tours/vietnam-muslim-tours/">Vietnam Muslim Tours</a></li><li id="menu-item-1532" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1532"><a href="https://www.tnktravel.com/tours/vietnam-tours/vietnam-adventure-tours/">Vietnam Adventure Tours</a></li><li id="menu-item-5134" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5134"><a href="https://www.tnktravel.com/tours/vietnam-tours/halong-bay-cruises/">Halong Bay Cruises</a></li><li id="menu-item-5137" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5137"><a href="https://www.tnktravel.com/tours/vietnam-tours/trekking-kayaking-and-cycling/">Trekking Kayaking and Cycling</a></li><li id="menu-item-5138" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5138"><a href="https://www.tnktravel.com/tours/vietnam-tours/vietnam-beach-breaks/">Vietnam Beach Breaks</a></li><li id="menu-item-5139" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5139"><a href="https://www.tnktravel.com/tours/vietnam-tours/vietnam-day-trips/">Vietnam Day Trips</a></li><li id="menu-item-10698" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-10698"><a href="https://www.tnktravel.com/tours/vietnam-tours/saigon-day-trips/">Saigon Day Trips</a></li></ul><span class="expand-me"></span></li><li id="menu-item-5140" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5140"><a href="https://www.tnktravel.com/tours/laos-tours/">Laos Tours</a></li><li id="menu-item-5146" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5146"><a href="https://www.tnktravel.com/tours/cambodia-tours/">Cambodia Tours</a></li><li id="menu-item-5155" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5155"><a href="https://www.tnktravel.com/tours/myanmar-tours/">Myanmar Tours</a></li><li id="menu-item-5161" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5161"><a href="https://www.tnktravel.com/tours/thailand-tours/">Thailand Tours</a></li><li id="menu-item-5162" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5162"><a href="https://www.tnktravel.com/tours/multi-country-tours/">Multi Country Tours</a></li><li id="menu-item-15819" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15819"><a href="https://www.tnktravel.com/customized-tours/">Customized Tours</a></li><li id="menu-item-5163" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-5163 has-child"><a href="https://www.tnktravel.com/hotels/">Hotels</a><ul class="sub-menu" style="display: none;"><li id="menu-item-15818" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-15818 has-child"><a href="https://www.tnktravel.com/hotels/hotels-in-vietnam/">Hotels in Vietnam</a><ul class="sub-menu" style="display: none;"><li id="menu-item-15820" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-15820"><a href="https://www.tnktravel.com/hotels/hotels-in-vietnam/hotels-in-ho-chi-minh/">Hotels in Ho Chi Minh city</a></li></ul><span class="expand-me"></span></li><li id="menu-item-15817" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-15817"><a href="https://www.tnktravel.com/hotels/hotels-in-laos/">Hotels in Laos</a></li><li id="menu-item-5164" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5164"><a href="https://www.tnktravel.com/hotels/hotels-in-cambodia/">Hotels in Cambodia</a></li><li id="menu-item-5167" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5167"><a href="https://www.tnktravel.com/hotels/hotels-in-myanmar/">Hotels in Myanmar</a></li><li id="menu-item-5169" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5169"><a href="https://www.tnktravel.com/hotels/hotels-in-thai-lan/">Hotels in Thailand</a></li></ul><span class="expand-me"></span></li></ul>                        </div></div><div class="header-logo logo-mobile">
<a href="https://www.tnktravel.com">
<img src="https://www.tnktravel.com/wp-content/themes/default_v4/images/logo.png" alt="TNKTRAVEL">
</a></div></div></div></div>
</header>

<div id="travelo-signup" class="travelo-modal-box travelo-box">
    <div class="login-social">
        <a href="#" class="button login-facebook"><i
class="soap-icon-facebook"></i>Login with Facebook</a>
        <a href="#" class="button login-googleplus"><i
class="soap-icon-googleplus"></i>Login with Google+</a></div>
    <div class="seperator"><label>OR</label></div>
    <div class="simple-signup">
        <div class="text-center signup-email-section">
            <a href="#" class="signup-email"><i
class="soap-icon-letter"></i>Sign up with Email</a></div>
        <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund olicy, and Host Guarantee Terms.</p>
    </div>
    <div class="email-signup">
        <form name="registerform" action="#" method="post">
            <div class="form-group">
                <input type="email" name="user_email" tabindex="1" class="input-text full-width" placeholder="email address" /></div>
            <div class="form-group">
                <input type="password" name="user_pwd" tabindex="2" class="input-text full-width" placeholder="password" /></div>
            <div class="form-group">
                <input type="password" name="user_pwd_confirm" tabindex="3" class="input-text full-width" placeholder="confirm password" /></div>
            <div class="form-group">
                <div class="checkbox">
                    <label>
<input
type="checkbox" name="subcrible" tabindex="4"> Tell me about TNK Travel news
</label></div>
            </div>
            <div class="form-group">
                <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund Policy, and Host Guarantee Terms.</p>
            </div>
            <button type="submit" class="full-width btn-medium">SIGNUP</button></form>
    </div>
    <div class="seperator"></div>
    <p>Already a member? <a href="#travelo-login" class="goto-login soap-popupbox">Login</a></p>
</div>

<div id="travelo-login" class="travelo-modal-box travelo-box">
    <div class="login-social">
        <a href="#" class="button login-facebook"><i
class="soap-icon-facebook"></i>Login with Facebook</a>
        <a href="#" class="button login-googleplus"><i
class="soap-icon-googleplus"></i>Login with Google+</a></div>
    <div class="seperator"><label>OR</label></div>
    <form name="loginform" action="#" method="post">
        <div class="form-group">
            <input type="email" name="user_email" tabindex="1" class="input-text full-width" placeholder="email address" /></div>
        <div class="form-group">
            <input type="password" name="user_pwd" tabindex="2" class="input-text full-width" placeholder="password"></div>
        <div class="form-group">
            <a href="#travelo-lost-password" class="forgot-password soap-popupbox pull-right">Forgot password?</a>
            <div class="checkbox checkbox-inline">
                <label>
<input
type="checkbox" name="rememberme" tabindex="3" value="forever"> Remember me
</label></div>
        </div>
        <div class="form-group">
            <button tabindex="4" class="button btn-medium btn-login full-width">LOG IN</button></div>
        <input type="hidden" name="redirect_to" value="http://www.soaptheme.net/wordpress/travelo"></form>
    <div class="seperator"></div>
    <p>Don't have an account? <a href="#travelo-signup" class="goto-signup soap-popupbox">Sign up</a></p>
</div>

<div id="travelo-lost-password" class="travelo-modal-box travelo-box">
    <form name="forgot-password-form" action="#" method="post">
        <p style="text-align: center">Please enter your email address. You will receive a link to create a new password via email.</p>
        <div class="form-group" style="margin: 20px 0">
            <input type="email" name="user_email" tabindex="1" class="input-text full-width" placeholder="email address" /></div>
        <div class="form-group">
            <button tabindex="4" class="button btn-medium btn-login full-width">RESET PASSWORD</button></div>
        <div class="btn-cancel">
            <a href="#travelo-login" class="soap-popupbox">Cancel</a></div>
    </form>
</div>

<div id="carousel-home-page" class="carousel carousel-fade slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        <div class="item active ">
            <a href="/tours/vietnam-tours/mekong-delta-cruises/mekong-superior-cruises/mekong-eyes-cruises/">
                <img src="<?= $TEMPLATE_SRC; ?>/data/home-slide-08.jpg" alt="...">
            </a>
        </div>
        <div class="item  ">
            <a href="/tours/vietnam-tours/vietnam-tour-packages/vietnam-classic-journey-12-days/">
                <img src="<?= $TEMPLATE_SRC; ?>/data/home-slide-06.jpg" alt="...">
            </a>
        </div>
        <div class="item  ">
            <a href="/tours/vietnam-tours/halongbaycruises/luxury-cruises/signature-cruise/">
                <img src="<?= $TEMPLATE_SRC; ?>/data/home-slide-07.jpg" alt="...">
            </a>
        </div>
        <div class="item  ">
            <a href="/tours/vietnam-tours/vietnam-tour-packages/the-road-of-the-world-heritage-6-days/">
                <img src="<?= $TEMPLATE_SRC; ?>/data/home-slide-09.jpg" alt="...">
            </a>
        </div>
    </div>
    <a class="left carousel-control" href="#carousel-home-page" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-home-page" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>


<section id="content" class="no-padding">
<div class="search-box-wrapper">
    <div class="search-box container">
        <ul class="search-tabs clearfix">
            <li class="active"><a href="#tours-tab" data-toggle="tab">TOURS</a></li>
            <li><a href="#hotels-tab" data-toggle="tab">HOTELS</a></li>
        </ul>
        <div class="visible-mobile">
            <ul id="mobile-search-tabs" class="search-tabs clearfix">
                <li class="active"><a href="#tours-tab" data-toggle="tab">TOURS</a></li>
                <li><a href="#hotels-tab" data-toggle="tab">HOTELS</a></li>
            </ul>
        </div>
        <div class="search-tab-content">
            <div class="tab-pane fade active in" id="tours-tab">
                <form role="search" method="post" id="frm_tour" class="tour-searchform" action="/tours/?cat=0" autocomplete="off">
                    <div class="overlay"></div>
                    <input type="hidden" name="sb" value="1" />
                    <div class="row">
                        <div class="form-group col-sm-6 col-md-3">
                            <label>Search tour name or trip code</label>
                            <div class="typeahead-container">
                                <input type="text" name="s" id="tour-search" class="input-text full-width" placeholder="" autocomplete="off" /></div>
                        </div>
                        <div class="form-group col-sm-6 col-md-4">
                            <div class="row">
                                <div class="col-xs-6">
                                    <label>Select destination</label>
                                    <div class="selector">
                                        <select name='cat' id='cat' class='postform'><option
value='0' selected='selected'>Select all</option>
</select></div>
                                </div>
                                <div class="col-xs-6">
                                    <label>Select tour type prefer</label>
                                    <div class="selector">
                                        <select name="tourtype" class="full-width"><option
value="0" selected>Select all</option>
</select></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-6 col-md-4">
                            <div class="row">
                                <div class="col-xs-6">
                                    <label>Your budget</label>
                                    <div class="selector">
                                        <select name="bud" class="full-width"><option
value="0">Any</option>                                               </select></div>
                                </div>
                                <div class="col-xs-6">
                                    <label>Tour length</label>
                                    <div class="selector">
                                        <select name="len" class="full-width"><option
value="0">Any</option>                                               </select></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-sm-6 col-md-1">
                            <label class="hidden-xs">&nbsp;</label>
                            <button type="submit" class="full-width icon-check animated" data-animation-type="bounce" data-animation-duration="1">SEARCH</button></div>

                    </div>
                </form>
            </div>

            <div class="tab-pane fade" id="hotels-tab">
                <form role="search" method="post" id="frm_hotel" action="/hotels/?cat=0" class="hotel-searchform" autocomplete="off">
                    <div class="overlay"></div>
                    <input type="hidden" name="sb" value="2" />
                    <div class="row">
                        <div class="form-group col-sm-6 col-md-3">
                            <label>Search hotel name</label>
                            <input type="text" name="k" class="input-text full-width" placeholder="" autocomplete="off" /></div>
                        <div class="form-group col-sm-6 col-md-4">
                            <div class="row">
                                <div class="col-xs-6">
                                    <label>Select country</label>
                                    <div class="selector">
                                        <select name="des" class="full-width"><option
value=0>Select country</option>                                               </select></div>
                                </div>
                                <div class="col-xs-6">
                                    <label>Select destination</label>
                                    <div class="selector">
                                        <select name="sub" class="full-width"><option
value=0>Select destination</option>
</select></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-6 col-md-4">
                            <div class="row">
                                <div class="col-xs-6">
                                    <label>Star ranking</label>
                                    <div class="selector">
                                        <select name="ran" class="full-width"><option
value="0">Ranking</option>                                              </select></div>
                                </div>
                                <div class="col-xs-6">
                                    <label>Hotel price</label>
                                    <div class="selector">
                                        <select name="pri" class="full-width"><option
value="0">Hotel price</option>                                              </select></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-6 col-md-1">
                            <label class="hidden-xs">&nbsp;</label>
                            <button type="submit" class="full-width icon-check animated" data-animation-type="bounce" data-animation-duration="1">SEARCH</button></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div
class="entry-content"><div
class="md-section"><div
class="container"><h2>The tours have been interested lately</h2>
<div class="row hotel image-box listing-style1">
<div class='col-sm-6 col-sms-6 col-md-3'>
    <article class="box">
        <figure class="animated" data-animation-type="fadeInDown" data-animation-duration="" data-animation-delay="0">
            <a href="#" data-post_id="45" class="hover-effect"><img src="<?= $TEMPLATE_SRC; ?>/CuChi-by-boat.jpg" alt="" /></a>
        </figure>
        <div class="details">
            <h4 class="box-title"><a href="#">Cu Chi Tunnels ( By Speed boat on the Saigon river )</a></h4>
            <div class="price1 clearfix">
                Price from <span class="price-new">$55</span> / per person
			</div>
            <p class="description">Normally tours to the Cu Chi tunnels are arranged by road. Now, you can visit the legendary tunnels by speedboat along ...</p>
            <div class="action clearfix">
                <a class="button btn-small" href="#">DETAILS</a>
                <a class="button btn-small yellow" href="#">ENQUIRE NOW</a></div>
        </div>
    </article>
</div>

<div class='col-sm-6 col-sms-6 col-md-3'>
    <article class="box">
        <figure class="animated" data-animation-type="fadeInDown" data-animation-duration="" data-animation-delay="0">
            <a href="#" data-post_id="45" class="hover-effect"><img src="<?= $TEMPLATE_SRC; ?>/CuChi-by-boat.jpg" alt="" /></a>
        </figure>
        <div class="details">
            <h4 class="box-title"><a href="#">Cu Chi Tunnels ( By Speed boat on the Saigon river )</a></h4>
            <div class="price1 clearfix">
                Price from <span class="price-new">$55</span> / per person
			</div>
            <p class="description">Normally tours to the Cu Chi tunnels are arranged by road. Now, you can visit the legendary tunnels by speedboat along ...</p>
            <div class="action clearfix">
                <a class="button btn-small" href="#">DETAILS</a>
                <a class="button btn-small yellow" href="#">ENQUIRE NOW</a></div>
        </div>
    </article>
</div>

<div class='col-sm-6 col-sms-6 col-md-3'>
    <article class="box">
        <figure class="animated" data-animation-type="fadeInDown" data-animation-duration="" data-animation-delay="0">
            <a href="#" data-post_id="45" class="hover-effect"><img src="<?= $TEMPLATE_SRC; ?>/CuChi-by-boat.jpg" alt="" /></a>
        </figure>
        <div class="details">
            <h4 class="box-title"><a href="#">Cu Chi Tunnels ( By Speed boat on the Saigon river )</a></h4>
            <div class="price1 clearfix">
                Price from <span class="price-new">$55</span> / per person
			</div>
            <p class="description">Normally tours to the Cu Chi tunnels are arranged by road. Now, you can visit the legendary tunnels by speedboat along ...</p>
            <div class="action clearfix">
                <a class="button btn-small" href="#">DETAILS</a>
                <a class="button btn-small yellow" href="#">ENQUIRE NOW</a></div>
        </div>
    </article>
</div>

<div class='col-sm-6 col-sms-6 col-md-3'>
    <article class="box">
        <figure class="animated" data-animation-type="fadeInDown" data-animation-duration="" data-animation-delay="0">
            <a href="#" data-post_id="45" class="hover-effect"><img src="<?= $TEMPLATE_SRC; ?>/CuChi-by-boat.jpg" alt="" /></a>
        </figure>
        <div class="details">
            <h4 class="box-title"><a href="#">Cu Chi Tunnels ( By Speed boat on the Saigon river )</a></h4>
            <div class="price1 clearfix">
                Price from <span class="price-new">$55</span> / per person
			</div>
            <p class="description">Normally tours to the Cu Chi tunnels are arranged by road. Now, you can visit the legendary tunnels by speedboat along ...</p>
            <div class="action clearfix">
                <a class="button btn-small" href="#">DETAILS</a>
                <a class="button btn-small yellow" href="#">ENQUIRE NOW</a></div>
        </div>
    </article>
</div>

</div></div></div>

<div class="xs-section no-padding-top">
    <div class="container">
        <h2>Feature tours</h2>
        <div class="block image-carousel style2 carousel_items" data-animation="slide" data-item-width="270" data-item-margin="30">
            <ul class="hotel image-box listing-style2 dt_carousel" data-items="4">
                <li class="column">
                    <article class="box">
                        <figure>
                            <a href="#" class="hover-effect popup-gallery"><img src="<?= $TEMPLATE_SRC; ?>/Thai-Lan-2.jpg" alt="" /></a>
                        </figure>
                        <div class="details">
                            <h4 class="box-title"><a href="#">Song Xanh Sampan Cruise</a></h4>
                            <div class="price1 clearfix">
                                Price from <span class="price-new">$229</span> / per person</div>
                            <p class="description">This tour is great for any individuals, couples as well as small groups who want to briefly explore the ...</p>
                            <div class="action clearfix">
                                <a class="button btn-small" href="#">DETAILS</a>
                                <a class="button btn-small yellow" href="#">ENQUIRE NOW</a>
							</div>
                        </div>
                    </article>
                </li>
                <li class="column">
                    <article class="box">
                        <figure>
                            <a href="#" class="hover-effect popup-gallery"><img src="<?= $TEMPLATE_SRC; ?>/Thai-Lan-2.jpg" alt="" /></a>
                        </figure>
                        <div class="details">
                            <h4 class="box-title"><a href="#">Song Xanh Sampan Cruise</a></h4>
                            <div class="price1 clearfix">
                                Price from <span class="price-new">$229</span> / per person</div>
                            <p class="description">This tour is great for any individuals, couples as well as small groups who want to briefly explore the ...</p>
                            <div class="action clearfix">
                                <a class="button btn-small" href="#">DETAILS</a>
                                <a class="button btn-small yellow" href="#">ENQUIRE NOW</a>
							</div>
                        </div>
                    </article>
                </li>
				<li class="column">
                    <article class="box">
                        <figure>
                            <a href="#" class="hover-effect popup-gallery"><img src="<?= $TEMPLATE_SRC; ?>/Thai-Lan-2.jpg" alt="" /></a>
                        </figure>
                        <div class="details">
                            <h4 class="box-title"><a href="#">Song Xanh Sampan Cruise</a></h4>
                            <div class="price1 clearfix">
                                Price from <span class="price-new">$229</span> / per person</div>
                            <p class="description">This tour is great for any individuals, couples as well as small groups who want to briefly explore the ...</p>
                            <div class="action clearfix">
                                <a class="button btn-small" href="#">DETAILS</a>
                                <a class="button btn-small yellow" href="#">ENQUIRE NOW</a>
							</div>
                        </div>
                    </article>
                </li>
				<li class="column">
                    <article class="box">
                        <figure>
                            <a href="#" class="hover-effect popup-gallery"><img src="<?= $TEMPLATE_SRC; ?>/Thai-Lan-2.jpg" alt="" /></a>
                        </figure>
                        <div class="details">
                            <h4 class="box-title"><a href="#">Song Xanh Sampan Cruise</a></h4>
                            <div class="price1 clearfix">
                                Price from <span class="price-new">$229</span> / per person</div>
                            <p class="description">This tour is great for any individuals, couples as well as small groups who want to briefly explore the ...</p>
                            <div class="action clearfix">
                                <a class="button btn-small" href="#">DETAILS</a>
                                <a class="button btn-small yellow" href="#">ENQUIRE NOW</a>
							</div>
                        </div>
                    </article>
                </li>
				<li class="column">
                    <article class="box">
                        <figure>
                            <a href="#" class="hover-effect popup-gallery"><img src="<?= $TEMPLATE_SRC; ?>/Thai-Lan-2.jpg" alt="" /></a>
                        </figure>
                        <div class="details">
                            <h4 class="box-title"><a href="#">Song Xanh Sampan Cruise</a></h4>
                            <div class="price1 clearfix">
                                Price from <span class="price-new">$229</span> / per person</div>
                            <p class="description">This tour is great for any individuals, couples as well as small groups who want to briefly explore the ...</p>
                            <div class="action clearfix">
                                <a class="button btn-small" href="#">DETAILS</a>
                                <a class="button btn-small yellow" href="#">ENQUIRE NOW</a>
							</div>
                        </div>
                    </article>
                </li>
            </ul>
            <div class="carousel-arrows">
                <button id="goToPrevSlide"><span
class="fa fa-angle-left"> </span></button>
                <button id="goToNextSlide"><span
class="fa fa-angle-right"> </span></button></div>
        </div>
    </div>
</div>

<div class="parallax global-map-area" data-stellar-background-ratio="0.5">
    <div class="md-section">
        <div class="container description">
            <div class="row">
                <div class="col-md-4 ">
                    <div class="services-lines-info">
                        <h2>Amazing Tour Style Ideas for
<em>you!</em></h2>
                        <p>Let’s start planning your long-awaited vacation with flights, hotels, car rentals, cruises and choose your travel style with TNK Travel.</p>
                    </div>
                </div>
                <div class="col-md-8 ">
                    <ul class="services-lines">
                        <li>
                            <div class="item-service-line animated" data-animation-type="slideInRight" data-animation-duration="2" data-animation-delay="0">
                                <a href="/tours/vietnam-tours/trekking-kayaking-and-cycling/">
                                    <i class="fa fa-bicycle yellow-color"></i>
                                    <h5>Trekking &amp; Cycling</h5>
                                </a>
                            </div>
                        </li>
                        <li>
                            <div class="item-service-line animated" data-animation-type="slideInRight" data-animation-duration="2" data-animation-delay="0.3">
                                <a href="/tours/vietnam-tours/vietnam-beach-breaks/">
                                    <i class="fa soap soap-icon-beach yellow-color"></i>
                                    <h5>Beach &amp; Vacations</h5>
                                </a>
                            </div>
                        </li>
                        <li>
                            <div class="item-service-line animated" data-animation-type="slideInRight" data-animation-duration="2" data-animation-delay="0.6">
                                <a href="/tours/vietnam-tours/vietnam-tour-packages/">
                                    <i class="fa soap soap-icon-trunk-2 yellow-color"></i>
                                    <h5>Tour Packages</h5>
                                </a>
                            </div>
                        </li>
                        <li>
                            <div class="item-service-line animated" data-animation-type="slideInRight" data-animation-duration="2" data-animation-delay="0.9">
                                <a href="/tours/vietnam-tours/vietnam-day-trips/">
                                    <i class="fa fa-bus yellow-color"></i>
                                    <h5>Day Trips</h5>
                                </a>
                            </div>
                        </li>
                        <li>
                            <div class="item-service-line animated" data-animation-type="slideInRight" data-animation-duration="2" data-animation-delay="1.2">
                                <a href="/tours/vietnam-tours/halongbaycruises/">
                                    <i class="fa soap soap-icon-cruise-2 yellow-color"></i>
                                    <h5>Halong Bay Cruises</h5>
                                </a>
                            </div>
                        </li>
                        <li>
                            <div class="item-service-line animated" data-animation-type="slideInRight" data-animation-duration="2" data-animation-delay="1.5">
                                <a href="/tours/vietnam-tours/mekong-delta-cruises/">
                                    <i class="fa soap soap-icon-cruise-3 yellow-color"></i>
                                    <h5>Mekong Delta Tours</h5>
                                </a>
                            </div>
                        </li>
                        <li>
                            <div class="item-service-line animated" data-animation-type="slideInRight" data-animation-duration="2" data-animation-delay="1.8">
                                <a href="/tours/vietnam-tours/vietnam-muslim-tours/">
                                    <i class="fa fa-bank yellow-color"></i>
                                    <h5>Muslim Tours</h5>
                                </a>
                            </div>
                        </li>
                        <li>
                            <div class="item-service-line animated" data-animation-type="slideInRight" data-animation-duration="2" data-animation-delay="2.1">
                                <a href="/tours/vietnam-tours/vietnam-adventure-tours/">
                                    <i class="fa soap-icon-adventure yellow-color"></i>
                                    <h5>Adventure Tours</h5>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section offer">
    <div class="container">
        <h2 class="text-center">OUR SERVICES</h2>
        <p class="col-xs-9 center-block no-float text-center" style="margin-bottom:50px;">Beside choosing your favorite destinations and tour packages. You can also check availability of other services with TNK Travel.</p>
        <div class="image-box style2">
            <div class="row">
                <div class="col-md-6">
                    <article class="box">
                        <figure>
                            <a href="/vietnam-visa-on-arrival/" class="hover-effect" title=""><img src="<?= $TEMPLATE_SRC; ?>/data/img_visa.jpg" alt="imageframe-image" width="270" height="192" class="animated" data-animation-type="fadeInLeft" data-animation-duration="1" data-animation-delay="0.4" /></a>
                        </figure>
                        <div class="details">
                            <p>Viet Nam Visa On Arrival which is very convenient for passengers because the lowest price, you no longer have to go to the Embassy or send your passport by post, safely, fastest, reliable....</p>
                            <a href="/vietnam-visa-on-arrival/" title="" class="button">SEE ALL</a></div>
                    </article>
                </div>
                <div class="col-md-6">
                    <article class="box">
                        <figure>
                            <a href="/car-rental/" class="hover-effect" title=""><img src="<?= $TEMPLATE_SRC; ?>/data/img_car.jpg" alt="imageframe-image" width="270" height="192" class="animated" data-animation-type="fadeInLeft" data-animation-duration="1" data-animation-delay="" /></a>
                        </figure>
                        <div class="details">
                            <p>Beside, being a local tours operator in Vietnam who offering many tour packages for individual trip and group tour. TNK Travel also offers car rental service in all major cities & airports, seaports from the North to South in Vietnam....</p>
                            <a href="/car-rental/" title="" class="button">SEE ALL</a></div>
                    </article>
                </div>
                <div class="col-md-6">
                    <article class="box">
                        <figure>
                            <a href="/flight-ticket-booking/" class="hover-effect" title=""><img src="<?= $TEMPLATE_SRC; ?>/data/img_flight.jpg" alt="imageframe-image" width="270" height="192" class="animated" data-animation-type="fadeInLeft" data-animation-duration="1" data-animation-delay="0.4" /></a>
                        </figure>
                        <div class="details">
                            <p>Join TNK Travel to get benefits, ranging from discount airfare to flight tickets for vocation packages.</p>
                            <a href="/flight-ticket-booking/" title="" class="button">SEE ALL</a></div>
                    </article>
                </div>
                <div class="col-md-6">
                    <article class="box">
                        <figure>
                            <a href="/train-ticket-booking/" class="hover-effect" title=""><img src="<?= $TEMPLATE_SRC; ?>/data/img_train.jpg" alt="imageframe-image" width="270" height="192" class="animated" data-animation-type="fadeInLeft" data-animation-duration="1" data-animation-delay="" /></a>
                        </figure>
                        <div class="details">
                            <p>One of the best ways to travel in Vietnam from the North to the South is by train. The Vietnam's railway network serves a large swathe of the country and extends to most destinations of interest to a first-time visitor in Vietnam....</p>
                            <a href="/train-ticket-booking/" title="" class="button">SEE ALL</a></div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section no-padding-top">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <h2 class="title-feature">
<i
class="fa fa-thumbs-o-up"></i>
WHY CHOOSE US <span>?</span></h2>
                <ul class="why-choice-us list-4">
                    <li>International tour operator.</li>
                    <li>Dedicated team of travel experts.</li>
                    <li>Flexibility and responsibility.</li>
                    <li>Unbeatable prices and unsurpassed value.</li>
                    <li>Guarantee highest quality of service.</li>
                    <li>15 years of travel industry experience.</li>
                    <li>Making your holiday "a journey of a lifetime".</li>
                </ul>
            </div>
            <div class="col-sm-7">
                <h2 class="title-feature">
<i
class="fa fa-comments-o"></i>
TESTIMONIALS</h2>
                <div id="carousel-testimonial" class="carousel carousel-fade slide">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-testimonial" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-testimonial" data-slide-to="1"></li>
                        <li data-target="#carousel-testimonial" data-slide-to="2"></li>
                        <li data-target="#carousel-testimonial" data-slide-to="3"></li>
                        <li data-target="#carousel-testimonial" data-slide-to="4"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="testimonial-inner-content">
                                The Mekong Delta, three day tour, was excellent. <br /> The tour guide Cong, not sure of spelling, also known as Max, was organised, professional, and friendly.<br /> I had some unforgettable experiences on this tour, and the Mekong River, is so beautiful.<br /> I would reccommend it to friends and family.</div>
                            <div class="testimonial-gimmick"></div>
                            <div class="clear"></div>
                            <div class="testimonial-info">
                                <span class="testimonial-author">Deborah Jane Cairns<br
/>South Africa</span>
                                <small class="testimonial-time">Dec 28, 2015</small></div>
                        </div>
                        <div class="item ">
                            <div class="testimonial-inner-content">
                                We took a tour too Cu Chi. The tour was fantastic. Our tour guide was An. He was absolutely amazing with incredible knowledge about Vietnam history, with a great sense of humor. He was genuine and very kind. He made the tour 100% times better. If anyone is going on a tour, I would highly recommend An as your tour guide!</div>
                            <div class="testimonial-gimmick"></div>
                            <div class="clear"></div>
                            <div class="testimonial-info">
                                <span class="testimonial-author">Sam Ly Doan<br
/>Canada</span>
                                <small class="testimonial-time">Oct 31, 2015</small></div>
                        </div>
                        <div class="item ">
                            <div class="testimonial-inner-content">
                                It was disappointing that we booked and paid for our tour and then found on the tour this did not include the ticket to get into Cu Chi. This detracted from the experience and was poor form not to have this explained to us prior.</div>
                            <div class="testimonial-gimmick"></div>
                            <div class="clear"></div>
                            <div class="testimonial-info">
                                <span class="testimonial-author">Mark Scown<br
/>New Zealand</span>
                                <small class="testimonial-time">Oct 6, 2015</small></div>
                        </div>
                        <div class="item ">
                            <div class="testimonial-inner-content">
                                We had a great day today, at the war museum, the palese and the most fun where the Cu Chi tunnels, and we had a really friendly guide who speaks perfect english.</div>
                            <div class="testimonial-gimmick"></div>
                            <div class="clear"></div>
                            <div class="testimonial-info">
                                <span class="testimonial-author">Martijn and Kim<br
/>United States</span>
                                <small class="testimonial-time">Aug 27, 2015</small></div>
                        </div>
                        <div class="item ">
                            <div class="testimonial-inner-content">
                                <strong>Another satisfied tour with TNK!</strong><br />
                                <br /> This was our 2nd trip to <strong>Vietnam with TNK Travel</strong>. We love everything arranged by experts. The guide, driver, mini bus and hotel were booked and timing. We were very strict on our Halah food restaurants. They have been conscious about this and well arrange every meal for us. Special thanks to Ms Luu Vo ( travel consultant ) and AK ( our guide )who have been working hard and go beyond to help us. We were very impressed with the services offered by these friendly staffs. Thanks for all your efforts and good luck to your agents. Go Vietnam, travel with TNK for on a customized tour. You will be received like VIPs!</div>
                            <div class="testimonial-gimmick"></div>
                            <div class="clear"></div>
                            <div class="testimonial-info">
                                <span class="testimonial-author">Rosland<br
/>Malaysia</span>
                                <small class="testimonial-time">Sep 27, 2014</small></div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>

</div>
</section>

<div class="newsletter hidden">
    <div class="container">
        <div class="col-md-6">
            <h3><i
class="fa fa-newspaper-o"></i>
Deals &amp; Discounts</h3> Enter your email address and we’ll send you our regular promotional emails, packed with special offers, great deals, and huge discounts</div>
        <div class="col-md-6">
            <div class="subscribe-form">
                <form method="get" action="#">
                    <input type="text" class="subscribe-input" placeholder="Your email" value="" name="">
                    <button class="btn-register awe-btn-5 arrow-right text-uppercase awe-btn-lager" type="submit">subcrible</button></form>
            </div>
            <span class="unsubscribe-tooltip" data-toggle="tooltip" data-placement="bottom" data-content="You can unsubscribe at any time at tnktravel.com/unsubscribe or by sending an email to <a href='mailto:unsubscribe@tnktravel.com'>unsubscribe@tnktravel.com</a>">How to unsubscribe?</span></div>
    </div>
</div>

<footer>
    <div class="container" style="padding-bottom: 20px;">
        <div class="connectlink hidden">
            <a rel="nofollow" target="_blank" href=""><img alt="TRIPPY" src=""></a>
        </div>
        <div class="row bottom-content">
            <div class="col-md-5">
                <h3 class="bottom-col-title">HEAD OFFICE</h3>
                <div class="bottom-text">
                    <p><strong>International Touroperator License No: 79-102/2010/TCDL_GP LHQT</strong><br />
                        <strong>Main Office</strong>: 220 De Tham st., Dist. 1, Ho Chi Minh city, Vietnam<br />
                        <strong>In Hanoi</strong>: 13 Nguyen Huu Huan st., Hoan Kiem dist., Ha Noi capital, Vietnam<br />
                        <strong>Phone</strong>: (84-8) 3920 4766 (16 lines) &#8211; <strong>Fax</strong>: (84-8) 3920 5377<br />
                        <strong>E-mail</strong>: info@tnktravel.com &#8211; <strong>Website</strong>: <a href="https://www.tnktravel.com">https://www.tnktravel.com</a></p>
                    <ul class="social-icons" style="margin-top: 10px">
                        <li class="twitter"><a title="twitter" href="https://twitter.com/tnk_travel " target="_blank" data-toggle="tooltip"><i
class="soap-icon-twitter"></i></a></li>
                        <li class="googleplus"><a title="googleplus" href="https://plus.google.com/+tnktravel/posts" target="_blank" data-toggle="tooltip"><i
class="soap-icon-googleplus"></i></a></li>
                        <li class="facebook"><a title="facebook" href="https://www.facebook.com/TnkTravelVietnam/" target="_blank" data-toggle="tooltip"><i
class="soap-icon-facebook"></i></a></li>
                        <li class="youtube"><a title="youtube" href="https://youtube.com/user/tnktravel " target="_blank" data-toggle="tooltip"><i
class="soap-icon-youtube"></i></a></li>
                        <li class="pinterest"><a title="pinterest" href="https://www.pinterest.com/tnktravel/" target="_blank" data-toggle="tooltip"><i
class="soap-icon-pinterest"></i></a></li>
                        <li class="tumblr"><a title="tumblr" href="http://tnktravel.tumblr.com/" target="_blank" data-toggle="tooltip"><i
class="soap-icon-tumblr"></i></a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-3 hidden">
                <h3 class="bottom-col-title">ACCEPT PAYPAL</h3>
                <div class="bottom-text">
                    <a href="#" rel="nofollow" target="_blank" style="float: left;">
                        <img src="<?= $TEMPLATE_SRC; ?>/images/icon_paypal.png" width="73" height="75" alt="Official PayPal Seal" /></a>
                    <a href="javascript:;" onclick="call_DiaLog();" style="display: inline-block; margin: 15px 0px 0px 10px;">
                        <img src="<?= $TEMPLATE_SRC; ?>/images/icon_paynow.png" /></a>
                </div>
            </div>
            <div class="col-md-4 hidden">
                <h3 class="bottom-col-title">OUR AWARDS</h3>
                <div class="bottom-text">
                    <div class="topten">
                        <div id="TA_certificateOfExcellence354" class="TA_certificateOfExcellence" style="float:left;">
                            <ul id="vwYf6y" class="TA_links IMv7JqLdA">
                                <li id="yiSp6vqT" class="hjNGouzO5oOV">
                                    
                                </li>
                            </ul>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="footer-bg" class="footer_text">
        <div class="container">
            <div class="copyright pull-left">
                Copyright &copy; <strong>Vietnam Muslim Tour Kaka</strong>. All rights reserved.
            </div>
        </div>
    </div>
</footer>
</div>
<div id="bgDialog"></div>
<script type='text/javascript' src='<?= $TEMPLATE_SRC; ?>/js/skip-link-focus-fix.js'></script>
<script type='text/javascript' src='<?= $TEMPLATE_SRC; ?>/js/jquery.package.min.js'></script>
<script type='text/javascript' src='<?= $TEMPLATE_SRC; ?>/js/custom.js'></script>
</body>
</html>