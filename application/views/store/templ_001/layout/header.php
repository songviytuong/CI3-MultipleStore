<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?= $SITE_NAME; ?></title>
    <meta name="description" content="<?= $META_DESCRIPTION; ?>" />
    <link rel="icon" href="<?= $TEMPLATE_SRC; ?>/images/favicon.png">
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,300italic,400italic,500' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,100italic,500,400italic,400,300italic,300,500italic,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?= $TEMPLATE_SRC; ?>/css/font-awesome.min.css">
    <link href="<?= $TEMPLATE_SRC; ?>/css/bootstrap.min.css" rel="stylesheet">
    <!-- Owl Carousel Assets -->
    <link href="<?= $TEMPLATE_SRC; ?>/css/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="<?= $TEMPLATE_SRC; ?>/css/owl-carousel/owl.theme.css" rel="stylesheet">
    <link href="<?= $TEMPLATE_SRC; ?>/css/layout.css" rel="stylesheet">
    <link rel="stylesheet" id="jssDefault" href="<?= $TEMPLATE_SRC; ?>/css/colors/orange.css">
    <!-- Open Graph data -->
    <meta property="og:title" content="Your Title Here" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="https://example.com/" />
    <!-- og:images :  Minimum 200 x 200px, Over 1200 x 630px recommended -->
    <meta property="og:image" content="https://example.com/image.jpg" />
    <meta property="og:description" content="Your Description Here" />
    <meta property="og:site_name" content="Your Site Name, i.e. Inkwell" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="Your @publisher_handle">
    <meta name="twitter:title" content="Your Page Title">
    <meta name="twitter:description" content="Your Page descriptionless than 200 characters">
    <meta name="twitter:creator" content="Your @author_handle">
    <!-- Twitter Summary card images must be at least 120x120px -->
    <meta name="twitter:image" content="https://example.com/image.jpg">
    <script type="text/javascript">
        var _fullPath = '<?= $TEMPLATE_SRC; ?>/css/colors/';
    </script>
</head>