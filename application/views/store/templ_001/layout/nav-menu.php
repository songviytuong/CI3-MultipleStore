<ul class="nav-menu">
    <?php
        foreach($MainMenu as $nav){
            $href = $nav->Name.PAGE_EXTENSION;
    ?>
    <li>
        <a<?=($nav->Name == $menu_active) ? ' class="active"':'' ?> href="<?=$href;?>">
            <h2><?=$this->lang->line($nav->Name);?></h2>
        </a>
    </li>
    <?php } ?>
</ul>