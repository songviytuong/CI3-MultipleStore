<header>
    <div class="hdshadow">
        <div class="container sub-contnr hdwrap">
            <div class="row row-fluid hdcontnr mrgt">

                <div class="col-lg-3 col-sm-2 col-xs-3 span2 spnwdh">
                    <div class="logo lgwth">
                        <a href="https://www.indianholiday.com" class="lgnon"><img src="<?php echo THEMES_URL; ?>/images/img_trans.gif" width="1" height="1" class="ihpllogo" alt="Indian Holiday" /></a>

                        <a href="/" class="lgnon1"><img src="<?php echo THEMES_URL; ?>/images/logo-ihpl.png" width="64" height="41" class="img-responsive" alt="Indian Holiday Pvt Ltd Logo" /></a>
                    </div>
                </div>

                <div class="col-lg-9 col-sm-10 col-xs-9 top-rgthd span10 spnwdh1">

                    <div class="row">
                        <div class="col-lg-9 col-md-9 col-sm-6 col-xs-3 span10 cllwth" style="padding-right:0;">

                            <div class="call-optbx">
                                <div class="row">
                                    <div class="col-md-8 col-sm-4 span9 call-icon"><img src="<?php echo THEMES_URL; ?>/images/call-us-icon.png" width="40" height="40" class="img-responsive" alt="" /></div>

                                    <div class="col-md-4 col-sm-8 span3 call-txtbx">
                                        <i>Call Our Travel Experts</i>
                                        <p>Select Country</p>
                                    </div>
                                </div>
                                <span class="arrow"></span>
                            </div>

                        </div>

                        <div class="cmsite">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-7 span2 stwth">
                                <div class="global-sitesbx">

                                    <select name="countries" id="countries_dd" class="form-control" onclick="closeCallBox()">
                                        <option value='/' selected  data-title="Global">Global Sites</option>
                                        <option value='/fr/'   data-image="<?php echo THEMES_URL; ?>/images/msdropdown/icons/blank.gif" data-imagecss="flag fr" data-title="France">France</option>
                                        <option value='/de/'    data-image="<?php echo THEMES_URL; ?>/images/msdropdown/icons/blank.gif" data-imagecss="flag de" data-title="Germany">Germany </option>
                                        <option value='/pt/'   data-image="<?php echo THEMES_URL; ?>/images/msdropdown/icons/blank.gif" data-imagecss="flag pt" data-title="Portugal">Portugal</option>
                                        <option value='RU' data-image="<?php echo THEMES_URL; ?>/images/msdropdown/icons/blank.gif" data-imagecss="flag ru" data-title="Russian">русский</option>
                                    </select>

                                </div>
                            </div>

                            <div class="col-sm-1 col-xs-2 nwth"><a class="new-menu-link" href="#"><img src="<?php echo THEMES_URL; ?>/images/nav-icon1.png" width="20" height="20" alt="" onClick="closeCallBox()" /></a></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div> 

</header>

<div class="call-dropdown">

    <div class="container sub-contnr cllbx-wth">
        <div class="row row-fluid">

            <div class="col-lg-12 span12 call-experts">Call Our Travel Experts</div>

            <div class="col-md-5 span5 call-numb">
                <div itemscope itemtype="http://schema.org/LocalBusiness">
                    <ul>
                        <li>North America <span itemprop="telephone"><a href="tel:+18037683100">+1 803 768 3100</a></span></li>
                        <li>South America <span itemprop="telephone"><a href="tel:+5511957116035">+55 1195 7116 035</a></span></li>
                        <li>Europe <span itemprop="telephone"><a href="tel:+442032390008">+44 203 239 0008</a></span></li>
                        <li>Australia / <br />New zealand <span itemprop="telephone"><a href="tel:+61282053100">+61 28 205 3100</a></span></li>
                        <li>Russia <span itemprop="telephone"><a href="tel:+79037174363">+7 9037 174 363</a></span></li>
                        <li>Kazakhstan <span itemprop="telephone"><a href="tel:+77784980837">+777 8498 0837</a></span></li>
                        <li>Uzbekistan <span itemprop="telephone"><a href="tel:+998977712416">+99 8977 712 416</a></span></li>
                        <li>Ukraine <span itemprop="telephone"><a href="tel:+380679929697">+380 679 929 697</a></span></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-7 span6 call-numb-othr">
                <p>Asia &amp; Rest of the world</p>
                <div class="calln-othr" itemscope itemtype="http://schema.org/LocalBusiness">
                    <ul>
                        <li>
                            <p>For Inbound &amp; Domestic Tours:
                                <i>(Foreign nationals, NRI &amp; India nationals)</i></p>
                            <span itemprop="telephone"><a href="tel:+911142423100">+91 11 4242 3100</a></span>
                        </li>
                        <li>
                            <p>For Outbound Tours:
                                <i>(Indian nationals visiting outside India)</i></p>
                            <span itemprop="telephone"><a href="tel:+911147773434">+91 11 4777 3434</a></span>
                        </li>
                        <li>
                            <strong>Emergency Contact:</strong>
                            <span itemprop="telephone"><a href="tel:+919818336340" class="clltm">+91 9818 336 340</a></span>
                        </li>
                    </ul>
                </div>

                <ul class="call-scolm">
                    <li><i class="cll-mail"></i> <script>(function (m, d, l, s) {
                            dm = m + "@" + d;
                            w = document;
                            if (l)
                                w.write('<a href="mailto:' + dm + '" ' + s + '>');
                            w.write(dm);
                            if (l)
                                w.write('</a>');
                        })
                                ('inquiry', 'indianholiday.com', 1, '');</script></li>
                </ul>
            </div>

        </div>
    </div>

    <div class="closebtt">Close <i></i></div>
</div>

<!--header end -->
<div class="clearbt"></div>
<!--nav start -->
<div class="menuwrapper cf navbg">
    <div class="navcontainer">

        <nav id="new-menu" class="new-menu">
            <ul>
                <li class="dnon"><a href="https://www.indianholiday.com"><span class="muhomeicon"></span></a></li>
                <li class="has-submenu"><a href="/tour-packages-india/">Themes <i></i></a><span class="clickmu"></span>
                    <ul class="dropdown-1col">
                        <li>
                            <div class="linknav">
                                <ul class="innerlink">
                                    <li><a href="/tour-packages-india/golden-triangle-tour.html">Golden Triangle Tours</a></li>
                                    <li><a href="/tour-packages-india/winter-destination.html">Winter Tours</a></li>
                                    <li><a href="/tour-packages-india/honeymoon-tours.html">Honeymoon Tours</a></li>                         
                                    <li><a href="/tour-packages-india/family-holidays.html">Family Holidays</a></li>
                                    <li><a href="/tour-packages-india/beach-vacations.html">Beach Vacations</a></li>
                                    <li><a href="/tour-packages-india/spa-ayurveda.html">Ayurveda Spa Tours</a></li>
                                    <li><a href="/tour-packages-india/wildlife.html">Wildlife Holidays</a></li>
                                    <li><a href="/luxury-tours/">Luxury Holidays</a></li>
                                    <li><a href="/tour-packages-india/india-for-first-timers.html">India for First Timers</a></li>
                                    <li><a href="/tour-packages-india/hill-station-tours.html">Hill Stations Tours</a></li>
                                    <li><a href="/tour-packages-india/kerala-backwaters-tour.html">Kerala Backwaters</a></li>
                                    <li><a href="/tour-packages-india/luxury-cruises.html">Cruise Holidays</a></li>
                                    <li><a href="/tour-packages-india/adventure-tours.html">Adventure Tours</a></li>
                                    <li><a href="/tour-packages-india/train.html">Train Tours</a></li>
                                    <li><a href="/tour-packages-india/pilgrimage-tours.html">Pilgrimage Tours</a></li>
                                    <li><a href="/day-tours/">Day Tours</a></li>
                                    <li><a href="/group-tours.html">Group Tours</a></li>
                                </ul>
                                <a href="/tour-packages-india/" class="muviewall">view all &raquo;</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="has-submenu"><a href="/packages-by-destinations/">Packages <i></i></a><span class="clickmu"></span>
                    <ul class="dropdown-2col">
                        <li>
                            <div class="linknav">
                                <ul class="innerlink">
                                    <li><a href="/tours-of-india/">India</a></li>
                                    <li><a href="/international-tours/">International</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="has-submenu"><a href="/packages-by-duration/">Duration <i></i></a><span class="clickmu"></span>
                    <ul class="dropdown-2col">
                        <li>
                            <div class="linknav">
                                <ul class="innerlink">
                                    <li><a href="/day-tours/">1 Day</a></li>
                                    <li><a href="/packages-by-duration/3-7-days-packages.html">1 - 7 Days</a></li>
                                    <li><a href="/packages-by-duration/8-15-days-packages.html">8 - 15 Days</a></li>
                                    <li><a href="/packages-by-duration/16-30-days-packages.html">More Than - 15 Days</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="has-submenu nav-colr"><a href="#">Specials <i></i></a><span class="clickmu"></span>
                    <ul class="dropdown-columns2">
                        <li>
                            <div class="navrightbx">
                                <div class="nrhtext"><a style="position:relative;" href="/special-deal/" onClick="ga('send', 'event', 'Special Deal Top Menu', 'Click', 'Menu Link');"><img src="<?php echo THEMES_URL; ?>/images/the-great-escpaes.jpg" width="245" height="145" alt="" /><span>Special Deals</span></a></div>

                                <div class="nrhtext"><a style="position:relative;" href="/winter-destination.html" onClick="ga('send', 'event', 'Winter Destinations', 'Click', 'Menu Link');"><img src="<?php echo THEMES_URL; ?>/images/destinations-menu.jpg" width="245" height="145" alt="" /><span>Winter Destinations</span></a></div>
                            </div>
                            <div class="navlkwrap1">
                                <div class="navtour-imgbx">
                                    <a href="/luxury-tours/" style="position:relative;">
                                        <span class="newmusprites newmuluxury"></span>
                                        <span class="muluxuryicon"></span> Luxury Holidays
                                    </a>
                                </div>
                                <div class="navtour-imgbx">
                                    <a href="/wildlife-india/" style="position:relative;">
                                        <span class="newmusprites newmuwildlife"></span>
                                        <span class="muwildlifeicon"></span> Wildlife in India
                                    </a>
                                </div>
                            </div>
                            <div class="navlkwrap1">
                                <div class="navtour-imgbx navcolor">
                                    <a href="/offers/" style="position:relative;">
                                        <span class="newmusprites newmuoffer"></span>
                                        <span class="muoffericon"></span> Special Offers
                                    </a>
                                </div>
                                <div class="navtour-imgbx">
                                    <a href="/where-is-hot/" style="position:relative;">
                                        <span class="newmusprites newmudestination"></span>
                                        <span class="muwhere-hot"></span> Where is Hot
                                    </a>
                                </div>
                            </div>
                            <div class="navlkwrap1">
                                <div class="navtour-imgbx">
                                    <a href="/best-of-india/" style="position:relative;">
                                        <span class="newmusprites newmu-tourm"></span>
                                        <span class="mutravelicon"></span> Best of India
                                    </a>
                                </div>
                                <div class="navtour-imgbx">
                                    <a href="/fairs-and-festivals/" style="position:relative;">
                                        <span class="newmusprites newmu-monsoondest"></span>
                                        <span class="besticon"></span> Fair &amp; Festivals
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="has-submenu"><a href="/weekend-getaways/">Weekend Getaways <i></i></a><span class="clickmu"></span>
                    <ul class="dropdown-2col">
                        <li>
                            <div class="linknav">
                                <ul class="innerlink">
                                    <li><a href="/weekend-getaways/from-delhi.html">From Delhi</a></li>
                                    <li><a href="/weekend-getaways/from-mumbai.html">From Mumbai</a></li>
                                    <li><a href="/weekend-getaways/from-bangalore.html">From Bangalore</a></li>
                                    <li><a href="/weekend-getaways/from-pune.html">From Pune</a></li>
                                    <li><a href="/weekend-getaways/from-hyderabad.html">From Hyderabad</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <li><a href="/hotels/">Hotels</a></li>
                <li class="has-submenu"><a href="/travel-guide/">Travel Guide <i></i></a><span class="clickmu"></span>
                    <ul class="dropdown-2col">
                        <li>
                            <div class="linknav">
                                <ul class="innerlink">
                                    <li><a href="/destinations/">Destinations</a></li>
                                    <li><a href="/tourist-attraction/">Attractions</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <li><a href="/spa-ayurveda-resorts.html">Ayurveda &amp; Spa <img src="<?php echo THEMES_URL; ?>/images/blink.gif" width="30" height="18" class="hideh" alt="" /></a></li>
                <li><a href="/unesco-world-heritage-site/">UNESCO Sites</a></li>           
            </ul>
        </nav>     
    </div>
</div>

<script>
    jQuery(document).ready(function ($) {

        $('body').addClass('js');
        var $menu = $('#menu-wrap'),
                $menulink = $('.menu-wrap-link'),
                $menuTrigger = $('.has-submenu-wrap > .clickbtt');

        $menulink.click(function (e) {
            e.preventDefault();
            $menulink.toggleClass('active');
            if ($menu.parent().css("right") == "-220px") {
                $menu.addClass('active');
                $menu.parent().animate({'right': '0'}, 300);
            } else {
                $menu.removeClass('active');
                $menu.parent().animate({'right': '-220px'}, 300);
            }
            if($menu.parent().css("right")){}
        });

        $menuTrigger.click(function (e) {
            e.preventDefault();
            var $this = $(this);
            $menuTrigger.not($this).removeClass('active');
            $menuTrigger.not($this).next('ul').removeClass('active');
            $this.toggleClass('active').next('ul').toggleClass('active');
        });
    });
</script>     

</div>
<div class="clearbt"></div>
<!--/sphider_noindex-->
<div id="mainWrap">
    <div style="clear:both;"></div>

    <div class="col-lg-12">
        <div class="row">
            <div class="httop-bannerbx" id="homeSearch">
                <img src="<?php echo THEMES_URL; ?>/images/hotelbanner-img.jpg" width="1349" height="337" alt="" />           
                <div class="httop-searchform">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-10 col-md-8 col-lg-8 col-sm-offset-1 col-md-offset-2 col-lg-offset-2 bght">
                                <div class="col-lg-12">
                                    <!--                                <form method="post" id="gi_search" name="gi_search" action="/hotels/hotels-search/" >
                                                                        <div class="row">
                                                                            <div class="col-xs-10 htshfiedl1">
                                                                                <input type="text" id="gi_destination_city" value="" name="query"  class="form-control htsearchlst" placeholder="Enter City, Area, Locality or Hotel Name (Worldwide)" /></div>
                                                                            <div class="col-xs-2 htshfiedl2">
                                                                                <input id="auto-suggest-input" class="txt_sWidget" disabled="disabled" value="City" autocomplete="off"  type="hidden"/>
                                                                                <input id="auto-suggest-option" class="txt_sWidget" disabled="disabled" value="0" autocomplete="off"  type="hidden"/>
                                                                                <input type="submit" name="submit" id="hm_srch" class="htsearch-topbtt" value="Search" />
                                                                            </div>
                                                                        </div>
                                                                    </form>-->

                                    <form method="post" id="gi_search" name="gi_search" action="/hotels/hotels-search/" >
                                        <div class="row">
                                            <div class="col-xs-10 htshfiedl1">
                                                <input class="form-control htsearchlst" type="text" id="gi_destination_city" value="" name="query" placeholder="Enter City, Area, Locality or Hotel Name..." autocomplete="off" />
                                            </div>
                                            <div class="col-xs-2 htshfiedl2">
                                                <input id="auto-suggest-input" class="txt_sWidget" disabled="disabled" value="City" autocomplete="off"  type="hidden"/>
                                                <input id="auto-suggest-option" class="txt_sWidget" disabled="disabled" value="0" autocomplete="off"  type="hidden"/>
                                                <input type="submit" class="htsearch-topbtt"  id="hm_srch" value="Search" />
                                            </div>
                                        </div>
                                    </form>

                                </div>
                                <div style="clear:both;"></div>

                                <div class="col-lg-12">
                                    <div class="ht_asuggest dn">
                                        <ul class="search_res_list"></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>           
            </div>
        </div>
    </div>

    <div class="bgcolor col-lg-12">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="featured-htwrap">
                        <div class="row">
                            <div class="col-md-9 col-sm-7"><h1 class="hthead-txt">Hotels in India</h1></div>

                            <div class="col-md-3 col-sm-5 text-center">
                                { <a href="/hotels/india-hotels-directory/" class="allhtdest">View All Destinations</a> }
                            </div> 
                        </div>

                        <div class="row">
                            <div class="col-lg-12">

                                <div class="htcity-selbx">
                                    <select class="form-control" name="dest" onchange="if (this.value)
                                                window.location.href = this.value">
                                        <option value="0">Select City</option>
                                        <option value="/hotels/agra.html">Agra</option><option value="/hotels/ahmedabad.html">Ahmedabad</option><option value="/hotels/ajmer.html">Ajmer</option><option value="/hotels/allahabad.html">Allahabad</option><option value="/hotels/alleppey.html">Alleppey</option><option value="/hotels/almora.html">Almora</option><option value="/hotels/alwar.html">Alwar</option><option value="/hotels/ambala.html">Ambala</option><option value="/hotels/amritsar.html">Amritsar</option><option value="/hotels/andaman-and-nicobar.html">Andaman and Nicobar Islands</option><option value="/hotels/andhra-pradesh.html">Andhra Pradesh</option><option value="/hotels/arunachal-pradesh.html">Arunachal Pradesh</option><option value="/hotels/assam.html">Assam</option><option value="/hotels/aurangabad.html">Aurangabad</option><option value="/hotels/ayodhya.html">Ayodhya</option><option value="/hotels/badami.html">Badami</option><option value="/hotels/badrinath.html">Badrinath</option><option value="/hotels/balrampur.html">Balrampur</option><option value="/hotels/bandhavgarh.html">Bandhavgarh</option><option value="/hotels/bandipur.html">Bandipur</option><option value="/hotels/bassi.html">Bassi</option><option value="/hotels/bharatpur.html">Bharatpur</option><option value="/hotels/bhilai.html">Bhilai</option><option value="/hotels/bhimtal.html">Bhimtal</option><option value="/hotels/bhopal.html">Bhopal</option><option value="/hotels/bhubaneswar.html">Bhubaneswar</option><option value="/hotels/bhuj.html">Bhuj</option><option value="/hotels/bihar.html">Bihar</option><option value="/hotels/bikaner.html">Bikaner</option><option value="/hotels/bilaspur.html">Bilaspur</option><option value="/hotels/binsar.html">Binsar</option><option value="/hotels/bodhgaya.html">Bodhgaya</option><option value="/hotels/bundi.html">Bundi</option><option value="/hotels/chail.html">Chail</option><option value="/hotels/chamba.html">Chamba</option><option value="/hotels/chandigarh.html">Chandigarh</option><option value="/hotels/chennai.html">Chennai</option><option value="/hotels/chettinad.html">Chettinad</option><option value="/hotels/chhattisgarh.html">Chhattisgarh</option><option value="/hotels/chhitorgarh.html">Chhitorgarh</option><option value="/hotels/chiplun.html">Chiplun</option><option value="/hotels/churu.html">Churu</option><option value="/hotels/kochi.html">Cochin (Kochi)</option><option value="/hotels/coimbatore.html">Coimbatore</option><option value="/hotels/colombo.html">Colombo</option><option value="/hotels/coonoor.html">Coonoor</option><option value="/hotels/coorg.html">Coorg</option><option value="/hotels/corbett.html">Corbett</option><option value="/hotels/cuttack.html">Cuttack</option><option value="/hotels/dalhousie.html">Dalhousie</option><option value="/hotels/daman.html">Daman</option><option value="/hotels/daman-diu.html">Daman Diu</option><option value="/hotels/dandeli.html">Dandeli</option><option value="/hotels/dantewada.html">Dantewada</option><option value="/hotels/darjeeling.html">Darjeeling</option><option value="/hotels/dehradun.html">Dehradun</option><option value="/hotels/deogarh.html">Deogarh</option><option value="/hotels/dharamshala.html">Dharamshala</option><option value="/hotels/dholpur.html">Dholpur</option><option value="/hotels/dibrugarh.html">Dibrugarh</option><option value="/hotels/digha.html">Digha</option><option value="/hotels/diu.html">Diu</option><option value="/hotels/diveagar.html">Diveagar</option><option value="/hotels/dundlod.html">Dundlod</option><option value="/hotels/durgapur.html">Durgapur</option><option value="/hotels/dwarka.html">Dwarka</option><option value="/hotels/faizabad.html">Faizabad</option><option value="/hotels/gajner.html">Gajner</option><option value="/hotels/gandhinagar.html">Gandhinagar</option><option value="/hotels/gangtok.html">Gangtok</option><option value="/hotels/gokarna.html">Gokarna</option><option value="/hotels/gujarat.html">Gujarat</option><option value="/hotels/gurgaon.html">Gurgaon</option><option value="/hotels/guruvayoor.html">Guruvayoor</option><option value="/hotels/guwahati.html">Guwahati</option><option value="/hotels/gwalior.html">Gwalior</option><option value="/hotels/haldia.html">Haldia</option><option value="/hotels/hampi.html">Hampi</option><option value="/hotels/haridwar.html">Haridwar</option><option value="/hotels/haryana.html">Haryana</option><option value="/hotels/hassan.html">Hassan</option><option value="/hotels/himachal-pradesh.html">Himachal Pradesh</option><option value="/hotels/hospet.html">Hospet</option><option value="/hotels/hosur.html">Hosur</option><option value="/hotels/hubli.html">Hubli</option><option value="/hotels/hyderabad.html">Hyderabad</option><option value="/hotels/indore.html">Indore</option><option value="/hotels/itanagar.html">Itanagar</option><option value="/hotels/jabalpur.html">Jabalpur</option><option value="/hotels/jaipur.html">Jaipur</option><option value="/hotels/jaisalmer.html">Jaisalmer</option><option value="/hotels/jammu.html">Jammu</option><option value="/hotels/jammu-kashmir.html">Jammu and Kashmir</option><option value="/hotels/jamnagar.html">Jamnagar</option><option value="/hotels/jamshedpur.html">Jamshedpur</option><option value="/hotels/Jeypore.html">Jeypore</option><option value="/hotels/jhansi.html">Jhansi</option><option value="/hotels/jharkhand.html">Jharkhand</option><option value="/hotels/jhunjhunu.html">Jhunjhunu</option><option value="/hotels/jodhpur.html">Jodhpur</option><option value="/hotels/junagadh.html">Junagadh</option><option value="/hotels/kabini.html">Kabini</option><option value="/hotels/kalimpong.html">Kalimpong</option><option value="/hotels/kanchipuram.html">Kanchipuram</option><option value="/hotels/kangra.html">Kangra</option><option value="/hotels/kanha.html">Kanha</option><option value="/hotels/kanker.html">Kanker</option><option value="/hotels/kannur.html">Kannur</option><option value="/hotels/kanpur.html">Kanpur</option><option value="/hotels/kanyakumari.html">Kanyakumari</option><option value="/hotels/karauli.html">Karauli</option><option value="/hotels/karnataka.html">Karnataka</option><option value="/hotels/kasargod.html">Kasargod</option><option value="/hotels/kasauli.html">Kasauli</option><option value="/hotels/katra.html">Katra</option><option value="/hotels/kausani.html">Kausani</option><option value="/hotels/kawardha.html">Kawardha </option><option value="/hotels/kaziranga.html">Kaziranga</option><option value="/hotels/kemmanagundi.html">Kemmanagundi</option><option value="/hotels/keonjhar.html">Keonjhar</option><option value="/hotels/kerala.html">Kerala</option><option value="/hotels/khajjiar.html">Khajjiar</option><option value="/hotels/khajuraho.html">Khajuraho</option><option value="/hotels/khandala.html">Khandala</option><option value="/hotels/khimsar.html">Khimsar</option><option value="/hotels/kishangarh.html">Kishangarh</option><option value="/hotels/kodaikanal.html">Kodaikanal</option><option value="/hotels/kohima.html">Kohima</option><option value="/hotels/kolkata.html">Kolkata</option><option value="/hotels/kollam.html">Kollam</option><option value="/hotels/konark.html">Konark</option><option value="/hotels/koriya.html">Koriya</option><option value="/hotels/kota.html">Kota</option><option value="/hotels/kottayam.html">Kottayam</option><option value="/hotels/kovalam.html">Kovalam</option><option value="/hotels/kozhikode.html">Kozhikode</option><option value="/hotels/kufri.html">Kufri</option><option value="/hotels/kumarakom.html">Kumarakom</option><option value="/hotels/kumbhalgarh.html">Kumbhalgarh</option><option value="/hotels/kurukshetra.html">Kurukshetra</option><option value="/hotels/kushinagar.html">Kushinagar</option><option value="/hotels/lakshadweep.html">Lakshadweep</option><option value="/hotels/leh-ladakh.html">Leh Ladakh</option><option value="/hotels/lonavala.html">Lonavala</option><option value="/hotels/lucknow.html">Lucknow</option><option value="/hotels/ludhiana.html">Ludhiana</option><option value="/hotels/madhya-pradesh.html">Madhya Pradesh</option><option value="/hotels/madikeri.html">Madikeri</option><option value="/hotels/madurai.html">Madurai</option><option value="/hotels/mahabaleshwar.html">Mahabaleshwar</option><option value="/hotels/mahabalipuram.html">Mahabalipuram</option><option value="/hotels/mahansar.html">Mahansar</option><option value="/hotels/maharashtra.html">Maharashtra</option><option value="/hotels/mahasamund.html">Mahasamund </option><option value="/hotels/malappuram.html">Malappuram</option><option value="/hotels/malshej-ghat.html">Malshej Ghat</option><option value="/hotels/manali.html">Manali</option><option value="/hotels/mandawa.html">Mandawa</option><option value="/hotels/mandawa-rajasthan.html">Mandawa Rajasthan</option><option value="/hotels/mandu.html">Mandu</option><option value="/hotels/mangalore.html">Mangalore</option><option value="/hotels/mararikulam.html">Mararikulam</option><option value="/hotels/mashobra.html">Mashobra</option><option value="/hotels/mashobra-himachal.html">Mashobra Himachal</option><option value="/hotels/matheran.html">Matheran</option><option value="/hotels/mathura.html">Mathura</option><option value="/hotels/meerut.html">Meerut</option><option value="/hotels/meghalaya.html">Meghalaya</option><option value="/hotels/mount-abu.html">Mount Abu</option><option value="/hotels/mukteshwar.html">Mukteshwar</option><option value="/hotels/mukundgarh.html">Mukundgarh</option><option value="/hotels/munnar.html">Munnar</option><option value="/hotels/mussoorie.html">Mussoorie </option><option value="/hotels/mysore.html">Mysore</option><option value="/hotels/nagaland.html">Nagaland</option><option value="/hotels/nagaur.html">Nagaur</option><option value="/hotels/nagpur.html">Nagpur</option><option value="/hotels/nainital.html">Nainital</option><option value="/hotels/nalagarh.html">Nalagarh</option><option value="/hotels/naldehra.html">Naldehra</option><option value="/hotels/narlai.html">Narlai</option><option value="/hotels/nashik.html">Nashik</option><option value="/hotels/nawalgarh.html">Nawalgarh</option><option value="/hotels/neemrana.html">Neemrana</option><option value="/hotels/noida.html">Noida</option><option value="/hotels/ooty.html">Ooty</option><option value="/hotels/orchha.html">Orchha</option><option value="/hotels/orissa.html">Orissa</option><option value="/hotels/pachmarhi.html">Pachmarhi</option><option value="/hotels/pachmarhi.html">Pachmarhi</option><option value="/hotels/palakkad.html">Palakkad</option><option value="/hotels/pali.html">Pali</option><option value="/hotels/palitana.html">Palitana</option><option value="/hotels/panchgani.html">Panchgani</option><option value="/hotels/parwanoo.html">Parwanoo</option><option value="/hotels/pathanamthitta.html">Pathanamthitta</option><option value="/hotels/patna.html">Patna</option><option value="/hotels/periyar.html">Periyar</option><option value="/hotels/periyar-kerala.html">Periyar Kerala</option><option value="/hotels/pondicherry.html">Pondicherry</option><option value="/hotels/poovar.html">Poovar</option><option value="/hotels/port-blair.html">Port Blair</option><option value="/hotels/pune.html">Pune</option><option value="/hotels/punjab.html">Punjab</option><option value="/hotels/puri.html">Puri</option><option value="/hotels/pushkar.html">Pushkar</option><option value="/hotels/puttaparthi.html">Puttaparthi</option><option value="/hotels/raipur.html">Raipur</option><option value="/hotels/rajasthan.html">Rajasthan</option><option value="/hotels/rajgir.html">Rajgir</option><option value="/hotels/rajkot.html">Rajkot</option><option value="/hotels/rajmachi.html">Rajmachi</option><option value="/hotels/rameshwaram.html">Rameswaram</option><option value="/hotels/ranakpur.html">Ranakpur</option><option value="/hotels/ranchi.html">Ranchi</option><option value="/hotels/ranikhet.html">Ranikhet</option><option value="/hotels/ranthambore.html">Ranthambore</option><option value="/hotels/rishikesh.html">Rishikesh</option><option value="/hotels/rohetgarh.html">Rohetgarh</option><option value="/hotels/rudraprayag.html">Rudraprayag</option><option value="/hotels/samode.html">Samode</option><option value="/hotels/sanchi.html">Sanchi</option><option value="/hotels/sariska.html">Sariska</option><option value="/hotels/sarnath.html">Sarnath</option><option value="/hotels/sasan-gir.html">Sasan Gir</option><option value="/hotels/shekhawati.html">Shekhawati</option><option value="/hotels/shillong.html">Shillong</option><option value="/hotels/shimla.html">Shimla</option><option value="/hotels/shirdi.html">Shirdi</option><option value="/hotels/shivpuri.html">Shivpuri</option><option value="/hotels/sikkim.html">Sikkim</option><option value="/hotels/siliguri.html">Siliguri</option><option value="/hotels/somnath.html">Somnath</option><option value="/hotels/sravasti.html">Sravasti</option><option value="/hotels/srinagar.html">Srinagar</option><option value="/hotels/subramanya.html">Subramanya</option><option value="/hotels/sundarbans.html">Sundarbans</option><option value="/hotels/surajgarh.html">Surajgarh</option><option value="/hotels/surat.html">Surat</option><option value="/hotels/tamilnadu.html">Tamil Nadu</option><option value="/hotels/tanjore.html">Tanjore</option><option value="/hotels/taragarh.html">Taragarh</option><option value="/hotels/thrissur.html">Thrissur</option><option value="/hotels/tirupati.html">Tirupati</option><option value="/hotels/trichy.html">Trichy</option><option value="/hotels/trivandrum.html">Trivandrum</option><option value="/hotels/udaipur.html">Udaipur</option><option value="/hotels/ujjain.html">Ujjain</option><option value="/hotels/uttar-pradesh.html">Uttar Pradesh</option><option value="/hotels/uttaranchal.html">Uttarakhand</option><option value="/hotels/vadodara.html">Vadodara</option><option value="/hotels/vapi.html">Vapi</option><option value="/hotels/varanasi.html">Varanasi</option><option value="/hotels/varkala.html">Varkala</option><option value="/hotels/vellore.html">Vellore</option><option value="/hotels/vijayawada.html">Vijayawada</option><option value="/hotels/visakhapatnam.html">Visakhapatnam</option><option value="/hotels/vrindavan.html">Vrindavan</option><option value="/hotels/vythiri.html">Vythiri</option><option value="/hotels/wayanad.html">Wayanad</option><option value="/hotels/west-bengal.html">West Bengal</option><option value="/hotels/yercaud.html">Yercaud</option><option value="/hotels/zainabad.html">Zainabad </option>                                    </select>
                                </div>

                                <ul class="nav nav-tabs hotel-tabwrap" role="tablist">
                                    <li class="tabLink active"><a href="#" onclick="javascript:return false;" id="cont-1" role="tab" data-toggle="tab" id="cont-1">New Delhi</a></li><li class="tabLink "><a href="#" onclick="javascript:return false;" id="cont-2" role="tab" data-toggle="tab" id="cont-2">Goa</a></li><li class="tabLink "><a href="#" onclick="javascript:return false;" id="cont-3" role="tab" data-toggle="tab" id="cont-3">Mumbai</a></li><li class="tabLink "><a href="#" onclick="javascript:return false;" id="cont-4" role="tab" data-toggle="tab" id="cont-4">Bangalore</a></li>                    		</ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="cont-1-1"><div class="hotel-tabcontent">
                                            <h3 class="hotel-tab">Hotel In New Delhi</h3>
                                            <ul>
                                                <li><a href="/hotels/the-imperial-hotel-new-delhi.html">The Imperial Hotel New Delhi</a>
                                                <td><img height="19" width="80"  src="<?php echo THEMES_URL; ?>/images/starlist2.png">
                                                    </li>
                                                <li><a href="/hotels/ashoka-hotel-new-delhi.html">Ashoka Hotel New Delhi</a>
                                                <td><img height="19" width="80"  src="<?php echo THEMES_URL; ?>/images/starlist2.png">
                                                    </li>
                                                <li><a href="/hotels/the-oberoi-hotel-new-delhi.html">The Oberoi Hotel New Delhi</a>
                                                <td><img height="19" width="80"  src="<?php echo THEMES_URL; ?>/images/starlist2.png">
                                                    </li>
                                                <li><a href="/hotels/the-leela-palace-chanakyapuri.html">The Leela Palace Chanakyapuri</a>
                                                <td><img height="19" width="80"  src="<?php echo THEMES_URL; ?>/images/starlist2.png">
                                                    </li>
                                            </ul>	
                                            <div class="clearfix"></div><a class="httabview" href="/hotels/delhi.html" >View All New Delhi Hotels</a>
                                        </div>
                                    </div><div class="tab-pane " id="cont-2-1"><div class="hotel-tabcontent">
                                            <h3 class="hotel-tab">Hotel In Goa</h3>
                                            <ul>
                                                <li><a href="/hotels/hotel-holiday-inn-south-goa.html">Hotel Holiday Inn South Goa</a>
                                                <td><img height="19" width="80"  src="<?php echo THEMES_URL; ?>/images/starlist2.png">
                                                    </li>
                                                <li><a href="/hotels/neelams-the-grand-goa.html">Neelam's The Grand</a>
                                                <td><img height="19" width="80"  src="<?php echo THEMES_URL; ?>/images/starlist4.png">
                                                    </li>
                                                <li><a href="/hotels/the-zuri-verca-white-sands-resort-goa-pedda-varca.html">The Zuri White Sands Resort Goa</a>
                                                <td><img height="19" width="80"  src="<?php echo THEMES_URL; ?>/images/starlist2.png">
                                                    </li>
                                                <li><a href="/hotels/the-leela-palace-goa.html">The Leela Palace Goa</a>
                                                <td><img height="19" width="80"  src="<?php echo THEMES_URL; ?>/images/starlist2.png">
                                                    </li>
                                            </ul>	
                                            <div class="clearfix"></div><a class="httabview" href="/hotels/goa.html" >View All Goa Hotels</a>
                                        </div>
                                    </div><div class="tab-pane " id="cont-3-1"><div class="hotel-tabcontent">
                                            <h3 class="hotel-tab">Hotel In Mumbai</h3>
                                            <ul>
                                                <li><a href="/hotels/itc-grand-central-mumbai.html">ITC Grand Central </a>
                                                <td><img height="19" width="80"  src="<?php echo THEMES_URL; ?>/images/starlist2.png">
                                                    </li>
                                                <li><a href="/hotels/taj-mahal-hotel-mumbai.html">Taj Mahal Palace Mumbai</a>
                                                <td><img height="19" width="80"  src="<?php echo THEMES_URL; ?>/images/starlist2.png">
                                                    </li>
                                                <li><a href="/hotels/taj-president-mumbai.html">Vivanta By Taj  President </a>
                                                <td><img height="19" width="80"  src="<?php echo THEMES_URL; ?>/images/starlist2.png">
                                                    </li>
                                                <li><a href="/hotels/the-oberoi-mumbai.html">The Oberoi Mumbai</a>
                                                <td><img height="19" width="80"  src="<?php echo THEMES_URL; ?>/images/starlist2.png">
                                                    </li>
                                            </ul>	
                                            <div class="clearfix"></div><a class="httabview" href="/hotels/mumbai.html" >View All Mumbai Hotels</a>
                                        </div>
                                    </div><div class="tab-pane " id="cont-4-1"><div class="hotel-tabcontent">
                                            <h3 class="hotel-tab">Hotel In Bangalore</h3>
                                            <ul>
                                                <li><a href="/hotels/the-park-hotel-in-bangalore.html">The Park Hotel Bangalore</a>
                                                <td><img height="19" width="80"  src="<?php echo THEMES_URL; ?>/images/starlist2.png">
                                                    </li>
                                                <li><a href="/hotels/hotel-ista-bangalore.html">Hyatt Bangalore M G Road </a>
                                                <td><img height="19" width="80"  src="<?php echo THEMES_URL; ?>/images/starlist2.png">
                                                    </li>
                                                <li><a href="/hotels/leela-palace-bangalore.html">Leela Palace Bangalore</a>
                                                <td><img height="19" width="80"  src="<?php echo THEMES_URL; ?>/images/starlist2.png">
                                                    </li>
                                                <li><a href="/hotels/the-itc-royal-gardenia-bangalore.html">The ITC Royal Gardenia</a>
                                                <td><img height="19" width="80"  src="<?php echo THEMES_URL; ?>/images/starlist2.png">
                                                    </li>
                                            </ul>	
                                            <div class="clearfix"></div><a class="httabview" href="/hotels/bangalore.html" >View All Bangalore Hotels</a>
                                        </div>
                                    </div>                            </div>

                            </div>
                        </div>
                    </div>
                </div>




            </div>
            <br />

            <div class="row">

                <div class="col-md-4">
                    <div class="hthead-txt1 text-center">Top Deals</div>

                    <div class="httop-dealbx">
                        <div class="media">
                            <a class="pull-left" href="/hotels/the-zuri-verca-white-sands-resort-goa-pedda-varca.html">
                                <img src="<?php echo THEMES_URL; ?>/images/hotels/hotel-main-page/Zuri-White-Sands-Resort-Goa.jpg" width="92" height="69" alt="" />
                            </a>
                            <div class="media-body httop-txtbx">
                                <a href="/hotels/the-zuri-verca-white-sands-resort-goa-pedda-varca.html" class="httop-namelk">The Zuri White Sands Resort Goa</a>
                                <!--<p>Janpath Lane, Connaught Place</p>-->
                                <img src="<?php echo THEMES_URL; ?>/images/hotels/hotel-main-page/starlist2.png" width="80" height="19" alt="" />
                                <a href="/hotels/the-zuri-verca-white-sands-resort-goa-pedda-varca.html" class="httop-alllk">view details</a>
                            </div>
                        </div>
                    </div>

                    <div class="httop-dealbx">
                        <div class="media">
                            <a class="pull-left" href="/hotels/hotel-holiday-inn-south-goa.html">
                                <img src="<?php echo THEMES_URL; ?>/images/hotels/hotel-main-page/Holiday-Inn-South-Goa.jpg" width="92" height="69" alt="" />
                            </a>
                            <div class="media-body httop-txtbx">
                                <a href="/hotels/hotel-holiday-inn-south-goa.html" class="httop-namelk">Hotel Holiday Inn South Goa</a>
                                <!--<p>Janpath Lane, Connaught Place</p>-->
                                <img src="<?php echo THEMES_URL; ?>/images/hotels/hotel-main-page/starlist2.png" width="80" height="19" alt="" />
                                <a href="/hotels/hotel-holiday-inn-south-goa.html" class="httop-alllk">view details</a>
                            </div>
                        </div>
                    </div>

                    <div class="httop-dealbx">
                        <div class="media">
                            <a class="pull-left" href="/hotels/taj-exotica-goa.html">
                                <img src="<?php echo THEMES_URL; ?>/images/hotels/hotel-main-page/Taj-Exotica-Goa.jpg" width="92" height="69" alt="" />
                            </a>
                            <div class="media-body httop-txtbx">
                                <a href="/hotels/taj-exotica-goa.html" class="httop-namelk">Taj Exotica Goa</a>
                                <!--<p>Janpath Lane, Connaught Place</p>-->
                                <img src="<?php echo THEMES_URL; ?>/images/hotels/hotel-main-page/starlist2.png" width="80" height="19" alt="" />
                                <a href="/hotels/taj-exotica-goa.html" class="httop-alllk">view details</a>
                            </div>
                        </div>
                    </div>

                </div>          


                <div class="col-md-5">
                    <div class="text-center"><h2 class="hthead-txt1">Popular Destinations</h2></div>

                    <div class="htpopl-destbx">
                        <ul class="list-inline">
                            <li><a href="/hotels/delhi.html">New Delhi</a></li>
                            <li><a href="/hotels/mumbai.html">Mumbai</a></li>
                            <li><a href="/hotels/goa.html">Goa</a></li>
                            <li><a href="/hotels/jaipur.html">Jaipur</a></li>
                            <li><a href="/hotels/jaisalmer.html">Jaisalmer</a></li>
                            <li><a href="/hotels/kovalam.html">Kovalam</a></li>
                            <li><a href="/hotels/varanasi.html">Varanasi</a></li>
                            <li><a href="/hotels/hyderabad.html">Hyderabad</a></li>
                            <li><a href="/hotels/shimla.html">Shimla</a></li>
                            <li><a href="/hotels/udaipur.html">Udaipur</a></li>
                            <li><a href="/hotels/kumarakom.html">Kumarakom</a></li>
                            <li><a href="/hotels/nainital.html">Nainital</a></li>
                            <li><a href="/hotels/kochi.html">Cochin</a></li>
                            <li><a href="/hotels/jammu-kashmir.html">Kashmir</a></li>
                            <li><a href="/hotels/alleppey.html">Alleppey</a></li>
                            <li><a href="/hotels/sikkim.html">Sikkim</a></li>
                            <li><a href="/hotels/mussoorie.html">Mussoorie</a></li>
                            <li><a href="/hotels/agra.html">Agra</a></li>
                            <li><a href="/hotels/chennai.html">Chennai</a></li>
                            <li><a href="/hotels/gujarat.html">Gujarat</a></li>
                            <li><a href="/hotels/kolkata.html">Kolkata</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-3 htbanners">
                    <a href="/tours-of-india/unforgettable-holidays-goan-village-resort.html" target="_blank">
                        <img src="<?php echo THEMES_URL; ?>/images/special-discount-goa.jpg" width="263" height="340" class="img-responsive" alt="" />
                    </a>
                </div>
            </div>

        </div>
        <br />
    </div>

    <div class="clearfix"></div>
    <br />

    <div class="container">
        <div class="row">
            <div class="text-center"><h2 class="hthead-txt">"Best Hotels In India" By Interest</h2></div>

            <div class="col-md-3 col-sm-4 col-xs-6 htbyinterest">
                <a href="/hotels/heritage-hotels.html">
                    <div class="htintinner intview">
                        <img src="<?php echo THEMES_URL; ?>/pictures/hotel/hoteltheme/Heritage Hotels-1.jpeg" width="262" height="175" class="img-responsive" alt="" />
                        <strong>Heritage Hotels</strong>
                        <div class="htcolorbx">
                            <span class="htthemeicon htheritage"></span>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 htbyinterest">
                <a href="/hotels/spa-hotel-resorts.html">
                    <div class="htintinner intview">
                        <img src="<?php echo THEMES_URL; ?>/pictures/hotel/hoteltheme/Spa-Ayurveda Hotel & Resorts-21.jpeg" width="262" height="175" class="img-responsive" alt="" />
                        <strong>Spa-Ayurveda Hotel & Resorts</strong>
                        <div class="htcolorbx">
                            <span class="htthemeicon htspa"></span>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 htbyinterest">
                <a href="/hotels/business-hotels.html">
                    <div class="htintinner intview">
                        <img src="<?php echo THEMES_URL; ?>/pictures/hotel/hoteltheme/Business Hotels-34.jpeg" width="262" height="175" class="img-responsive" alt="" />
                        <strong>Business Hotels</strong>
                        <div class="htcolorbx">
                            <span class="htthemeicon htbusiness"></span>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 htbyinterest">
                <a href="/hotels/wildlife-resorts.html">
                    <div class="htintinner intview">
                        <img src="<?php echo THEMES_URL; ?>/pictures/hotel/hoteltheme/Wildlife Resorts-32.jpeg" width="262" height="175" class="img-responsive" alt="" />
                        <strong>Wildlife Resorts</strong>
                        <div class="htcolorbx">
                            <span class="htthemeicon htwild"></span>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 htbyinterest">
                <a href="/hotels/beach-hotels-resorts.html">
                    <div class="htintinner intview">
                        <img src="<?php echo THEMES_URL; ?>/pictures/hotel/hoteltheme/Beach Hotels & Resorts-35.jpeg" width="262" height="175" class="img-responsive" alt="" />
                        <strong>Beach Hotels & Resorts</strong>
                        <div class="htcolorbx">
                            <span class="htthemeicon htbeach"></span>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 htbyinterest">
                <a href="/hotels/boutique-hotels.html">
                    <div class="htintinner intview">
                        <img src="<?php echo THEMES_URL; ?>/pictures/hotel/hoteltheme/Boutique Hotels-37.jpeg" width="262" height="175" class="img-responsive" alt="" />
                        <strong>Boutique Hotels</strong>
                        <div class="htcolorbx">
                            <span class="htthemeicon "></span>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 htbyinterest">
                <a href="/hotels/airport-hotels.html">
                    <div class="htintinner intview">
                        <img src="<?php echo THEMES_URL; ?>/pictures/hotel/hoteltheme/Airport Hotels-43.jpeg" width="262" height="175" class="img-responsive" alt="" />
                        <strong>Airport Hotels</strong>
                        <div class="htcolorbx">
                            <span class="htthemeicon "></span>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 htbyinterest">
                <a href="/hotels/honeymoon-resorts.html">
                    <div class="htintinner intview">
                        <img src="<?php echo THEMES_URL; ?>/pictures/hotel/hoteltheme/Honeymoon Resorts in India -44.jpeg" width="262" height="175" class="img-responsive" alt="" />
                        <strong>Honeymoon Resorts</strong>
                        <div class="htcolorbx">
                            <span class="htthemeicon hthoneymoon"></span>
                        </div>
                    </div>
                </a>
            </div>

        </div>
    </div>

    <div class="clearfix"></div>
    <br />

    <div class="bgcolor col-lg-12">
        <br />
        <div class="container">
            <div class="row">       
                <div class="col-md-9">
                    <div class="row">
                        <div class="text-center"><h2 class="hthead-txt">"Best Luxury Hotels" By Brand</h2></div>
                        <div class="col-sm-3 col-xs-6 htbrandbx"><a href="/hotels/hyatt-group-of-hotels.html"><img src="<?php echo THEMES_URL; ?>/pictures/hotel/hotelbrand/hyatt-group-of-hotels-51.png" width="160" height="80" class="img-responsive" alt="Hyatt Group Of Hotels" /></a></div>
                        <div class="col-sm-3 col-xs-6 htbrandbx"><a href="/hotels/the-lalit-group.html"><img src="<?php echo THEMES_URL; ?>/pictures/hotel/hotelbrand/the-lalit-group-56.png" width="160" height="80" class="img-responsive" alt="The Lalit Group" /></a></div>
                        <div class="col-sm-3 col-xs-6 htbrandbx"><a href="/hotels/itc-welcome-group.html"><img src="<?php echo THEMES_URL; ?>/pictures/hotel/hotelbrand/itc-welcome-group-57.png" width="160" height="80" class="img-responsive" alt="ITC Welcom Group" /></a></div>
                        <div class="col-sm-3 col-xs-6 htbrandbx"><a href="/hotels/jaypee-group-of-hotels.html"><img src="<?php echo THEMES_URL; ?>/pictures/hotel/hotelbrand/jaypee-group-of-hotels-60.png" width="160" height="80" class="img-responsive" alt="Jaypee Group Of Hotels" /></a></div>
                        <div class="col-sm-3 col-xs-6 htbrandbx"><a href="/hotels/oberoi-group-of-hotels.html"><img src="<?php echo THEMES_URL; ?>/pictures/hotel/hotelbrand/oberoi-group-of-hotels-86.png" width="160" height="80" class="img-responsive" alt="Oberoi Group Of Hotels" /></a></div>
                        <div class="col-sm-3 col-xs-6 htbrandbx"><a href="/hotels/taj-group-of-hotels.html"><img src="<?php echo THEMES_URL; ?>/pictures/hotel/hotelbrand/taj-group-of-hotels-108.png" width="160" height="80" class="img-responsive" alt="Taj Group Of Hotels" /></a></div>
                        <div class="col-sm-3 col-xs-6 htbrandbx"><a href="/hotels/the-leela-group-of-hotels.html"><img src="<?php echo THEMES_URL; ?>/pictures/hotel/hotelbrand/the-leela-group-of-hotels-115.png" width="160" height="80" class="img-responsive" alt="The Leela Group Of Hotels" /></a></div>
                        <div class="col-sm-3 col-xs-6 htbrandbx"><a href="/hotels/trident-hotels.html"><img src="<?php echo THEMES_URL; ?>/pictures/hotel/hotelbrand/trident-hotels-117.png" width="160" height="80" class="img-responsive" alt="Trident Hotels" /></a></div>

                    </div>


                </div>
                <div class="col-md-3 htbanners"><script>document.write('<a href="mailto:"+"inquiry"+"@"+"indianholiday.com"><img src="<?php echo THEMES_URL; ?>/images/partner-hotel.jpg" width="247" height="280" class="img-responsive" alt="" /></a>');</script></div>

            </div>
        </div>
        <br />
    </div>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".tabLink").each(function () {
                $(this).click(function () {
                    tabeId = $(this).find('a').attr('id');
                    $("ul").children('li.active').removeClass('active');
                    $(this).addClass("active");
                    $("div.tab-content").children('div.active').removeClass('active');
                    $("#" + tabeId + "-1").addClass("active");
                    return false;
                });
            });

            $('#hm_srch').click(function () {
                if ($('#gi_destination_city').val() == '') {
                    alert("Search box can't be blank");
                    return false;
                }
            });
        });
    </script>


    <!--sphider_noindex-->
    <div class="clearbt"></div>
</div>
</div>
<!--footer start -->
<br />
<footer id="footer">
    <br/>

    <div class="clearfix"></div>

    <footer id="wrapfooter">

        <div class="container sub-contnr footwrap">
            <div class="row row-fluid">
                <div class="col-lg-12 span12 back-btt"><!--a href="#">&laquo; Back</a-->

                    <script>
                        if (screen.width > 760)
                        {
                            var ex = document.referrer;
                            if (ex) {
                                document.write('<a href="#" onClick="GoBack(1); return false;" >Go Back</a>');
                            }
                        }
                        function GoBack(e) {
                            window.history.back();
                        }
                    </script>


                </div>
            </div>
        </div>

        <div class="footer-bg1 col-lg-12">
            <div class="container bottom-footer sub-contnr footwrap">
                <div class="row-fluid">
                    <ul class="footerlinks list-inline text-center">
                        <li><a href="/company.html">About Us</a></li> |
                        <li><a href="/contact-us.html">Contact Us</a></li> |
                        <li><a href="/blog/">Blog</a></li> |
                        <li><a href="/careers.html">Careers</a></li> | 
                        <li><a href="/sitemap.html">Site Map</a></li> |
                        <li><a href="/why-travel-with-us.html">Why IHPL</a></li> | 
                        <li><a href="/payment-procedure.html">Payment Procedure</a></li> |
                        <li><a href="/links.html">Resources</a></li> |
                        <li><a href="/terms-and-conditions.html">Terms &amp; Conditions</a></li> |
                        <li><a href="/visa-to-india.html">Visa to India</a></li> 
                    </ul>
                </div>
            </div>
        </div>

        <div class="footer-bg2 col-lg-12 text-center">
            <div class="container sub-contnr footwrap">
                <div class="row row-fluid norow">
                    <div class="col-lg-5 col-md-5 newsletter span5">
                        <form>
                            <label class="col-sm-3 control-label span3">Newsletter:</label>
                            <div class="col-sm-6 span6"><input type="email" class="form-control" placeholder="Email"></div>
                            <a href="#" id="newsletter" class="ask"><button type="button" class="btn btn-hot text-uppercase col-sm-2 span2 subbtt">Submit</button></a>
                        </form>
                    </div>

                    <div class="col-lg-7 col-md-7 span7 wrapsocial">

                        <div class="row norow">
                            <div class="col-lg-12 social-media span12 left-side">
                                <ul class="list-inline inline">
                                    <li><div class="slm-txt">Follow:</div></li>
                                    <li><a href="https://www.facebook.com/IndianHolidayPvtLtd" target="_blank" rel="nofollow"><span class="icn-facebook"></span></a></li>
                                    <li><a href="https://twitter.com/indianholiday" target="_blank" rel="nofollow"><span class="icn-twitter"></span></a></li>
                                    <li><a href="https://plus.google.com/+IndianHolidayPrivateLimitedNewDelhi" target="_blank" rel="nofollow"><span class="icn-googleplus"></span></a></li>
                                    <li><a href="http://www.linkedin.com/company/indian-holiday-pvt-ltd" target="_blank" rel="nofollow"><span class="icn-linkedin"></span></a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="footer-bg3 col-lg-12">
            <div class="container sub-contnr footwrap bottom-footer">
                <div class="row row-fluid norow">
                    <div class="col-lg-3 col-md-3 footer-address span3">
                        <div itemscope itemtype="https://schema.org/LocalBusiness">
                            <div class="heading"><span itemprop="name">Indian Holiday Pvt. Ltd.</span></div>
                            <div itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
                                <span itemprop="streetAddress">70, L.G.F, World Trade Centre</span>
                                <span itemprop="addressLocality">Barakhamba Lane, Connaught Place,</span>
                                <span itemprop="addressRegion">New Delhi - 110001 (India)</span>
                            </div>
                            Phone: <span itemprop="telephone">+84 989 466 466</span>
                        </div>
                        <div itemprop="location">
                            <span itemscope itemtype="https://schema.org/Place">
                                <div itemprop="geo">
                                    <span itemscope itemtype="https://schema.org/GeoCoordinates">
                                        <span property="latitude" content="28.631574"></span>
                                        <span property="longitude" content="77.226952"></span>
                                    </span>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 span9 footimg">
                        <img src="<?php echo THEMES_URL; ?>/images/footerbg.jpg" width="848" height="96" alt="" class="img-responsive" />
                        <p class="bottomtext">&copy; Indian Holiday Pvt. Ltd. Recognized by Ministry of Tourism, Government of India.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</footer>