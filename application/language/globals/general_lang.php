<?php

$CI = & get_instance();
$lang_id = $CI->cur_langid;
$CI->load->model('ind/iLanguages_model');
$arr_lang = $CI->iLanguages_model->getLanguageByLangID(intval($lang_id));
if (isset($arr_lang)) {
    foreach ($arr_lang as $item) {
        $lang[$item->Keyword] = $item->Translate;
    }
}