<?php

class iTemplates_model extends CI_Model {

    protected $IND_Table = 'ind_templates';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getTemplateInfo($TempID = 1) {
        $cacheId = __CLASS__ . '_' . __FUNCTION__ . '_' . 'TempID' . '_' . $TempID;
        $result = $this->cache->get($cacheId);
        if ($result === FALSE) {
            $this->load->database();
            $this->db->select('TempName');
            $this->db->where('ID', $TempID);
            $result = $this->db->get($this->IND_Table)->row();
            // Save into the cache
            $this->cache->save($cacheId, $result, _CACHE_TIME);
        }
        return $result;
    }

}
