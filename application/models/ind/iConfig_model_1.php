<?php

class iConfig_model extends CI_Model {

    protected $IND_db;
    protected $IND_Table = 'IND_Config';

    public function __construct() {
        parent::__construct();
        $CI = &get_instance();
        $this->IND_db = $CI->load->database('IND', TRUE);
    }

    function getConfig($orderby = 'Keyword', $sort = 'ASC') {
        $cacheId = __CLASS__ . '_getConfig_' . $orderby . '_' . $sort;
        $result = $this->cache->get($cacheId);
        if ($result === FALSE) {
            
            $this->IND_db->select('Keyword,Value');
            $this->IND_db->order_by($orderby, $sort);
            $result = $this->IND_db->get($this->IND_Table)->result_array();

            // Save into the cache
            $this->cache->save($cacheId, $result, _CACHE_TIME);
        }

        $res = array();
        if (!empty($result) && is_array($result)) {
            foreach ($result as $key => $item) {
                $res[$key]['Keyword'] = $item['Keyword'];
                if ($item['Value'] === "TRUE") {
                    $res[$key]['Value'] = TRUE;
                } else if ($item['Value'] === "FALSE") {
                    $res[$key]['Value'] = FALSE;
                } else {
                    $res[$key]['Value'] = $item['Value'];
                }
            }
        }
        return $res;
    }

}
