<?php

class iLanguages_model extends CI_Model {

    protected $IND_Table_Languages = 'ind_languages';
    protected $IND_Table_Languages_Translate = 'ind_languages_translate';

    function __construct() {
        parent::__construct();
    }

    function getLanguageByLangID($LangID = 2) {
        if ($LangID) {
            $cacheId = __CLASS__ . '_' . __FUNCTION__ . '_' . $LangID;
            $result = $this->cache->get($cacheId);
            if ($result === FALSE) {
                $this->load->database();
                $this->db->select('*');
                $this->db->where('LangID', $LangID);
                $result = $this->db->get($this->IND_Table_Languages_Translate)->result();
                // Save into the cache
                $this->cache->save($cacheId, $result, _CACHE_TIME);
            }return $result;
        }
    }

    function getLanguageBySession($LangName = "Vietnamese") {
        if ($LangName) {
            $cacheId = __CLASS__ . '_' . __FUNCTION__ . '_' . ucfirst($LangName);
            $result = $this->cache->get($cacheId);
            if ($result === FALSE) {
                $this->load->database();
                $this->db->select('*');
                $this->db->where('LangName', ucfirst($LangName));
                $result = $this->db->get($this->IND_Table_Languages)->row_array();
                // Save into the cache 
                $this->cache->save($cacheId, $result, _CACHE_TIME);
            }return $result;
        }
    }

    function getLangID($LangName) {
        $cacheId = __CLASS__ . '_' . __FUNCTION__ . '_' . $LangName;
        $result = $this->cache->get($cacheId);
        if ($result === FALSE) {
            $this->load->database();
            $this->db->select('LangID');
            $this->db->where('LangName', $LangName);
            $result = $this->db->get($this->IND_Table_Languages)->row();
            $result = $result->LangID;
            // Save into the cache
            $this->cache->save($cacheId, $result, _CACHE_TIME);
        }return $result->LangID;
    }

    public function getLangAliasByName($LangName = "Vietnamese") {
        $cacheId = __CLASS__ . '_' . __FUNCTION__ . '_' . $LangName;
        $result = $this->cache->get($cacheId);
        if ($result === FALSE) {
            $this->load->database();
            $this->db->select('LangAlias');
            $this->db->where('LangName', $LangName);
            $result = $this->db->get($this->IND_Table_Languages)->row();
            // Save into the cache
            $this->cache->save($cacheId, $result, _CACHE_TIME);
        }return $result->LangAlias;
    }

    function getLangNameByLocal($local) {
        $cacheId = __CLASS__ . '_' . __FUNCTION__ . '_' . $local;
        $result = $this->cache->get($cacheId);
        if ($result === FALSE) {
            $this->load->database();
            $this->db->select('LangID,LangName');
            $this->db->where('LangLocal', $local);
            $result = $this->db->get($this->IND_Table_Languages)->row_array();
            // Save into the cache
            $this->cache->save($cacheId, $result, _CACHE_TIME);
        }return $result;
    }

    function getAllLanguages($orderby = 'LangPosition', $sort = 'ASC') {
        $cacheId = __CLASS__ . '_' . __FUNCTION__ . '_' . $orderby . '_' . $sort;
        $result = $this->cache->get($cacheId);
        if ($result === FALSE) {
            $this->load->database();
            $this->db->select('*');
            $this->db->order_by($orderby, $sort);
            $result = $this->db->get($this->IND_Table_Languages)->result();
            // Save into the cache
            $this->cache->save($cacheId, $result, _CACHE_TIME);
        }return $result;
    }

    function defineAllLanguage() {
        $data = $this->getAllLanguages();
        $res = array();
        if ($data) {
            foreach ($data as $key => $row) {
                $res[$key]['ID'] = $row->LangID;
                $res[$key]['Local'] = $row->LangLocal;
                $res[$key]['Icon'] = $row->LangIcon;
                $res[$key]['Flag'] = $row->LangFlag;
                $res[$key]['Name'] = $row->LangName;
            }
        }
        return $res;
    }

    function getAllTranslates($orderby = 'LangID', $sort = 'DESC') {
        $cacheId = __CLASS__ . '_' . __FUNCTION__ . '_' . $orderby . '_' . $sort;
        $result = $this->cache->get($cacheId);
        if ($result === FALSE) {
            $this->load->database();
            $this->db->select('*');
            $this->db->order_by($orderby, $sort);
            $result = $this->db->get($this->IND_Table_Languages_Translate)->result();
            // Save into the cache
            $this->cache->save($cacheId, $result, _CACHE_TIME);
        }return $result;
    }

}
