<?php

class iSocial_model extends CI_Model {

    protected $IND_Table = 'ind_social';
    protected $IND_Table_Data = 'ind_social_data';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getSocialList($uid = 1) {
        $cacheId = __CLASS__ . '_' . __FUNCTION__ . '_' . 'UserID' . '_' . $uid;
        $result = $this->cache->get($cacheId);
        if ($result === FALSE) {
            $this->load->database();
            if ($uid) {
                $this->db->select('*');
                $this->db->from($this->IND_Table_Data);
                $this->db->join('ind_social', 'ind_social_data.SocialID = ind_social.ID');
                $this->db->where('ind_social_data.UserID', intval($uid));
                $this->db->where('ind_social_data.Active', 0);
                $result = $this->db->get()->result();
            } else {
                $this->db->select('*');
                $result = $this->db->get($this->IND_Table)->result();
            }
            // Save into the cache
            $this->cache->save($cacheId, $result, _CACHE_TIME);
        }
        return $result;
    }

}
