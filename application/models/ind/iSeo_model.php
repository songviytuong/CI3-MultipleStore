<?php

class iSeo_model extends CI_Model {

    protected $IND_Table = 'ind_seo';

    public function __construct() {
        parent::__construct();
    }

    public function getSEODefault($id = 1) {
        $cacheId = __CLASS__ . '_' . __FUNCTION__;
        $result = $this->cache->get($cacheId);
        if ($result === FALSE) {
            $this->load->database();
            $this->db->select('*');
            $this->db->where('ID', $id);
            $result = $this->db->get($this->IND_Table)->row_array();
            // Save into the cache
            $this->cache->save($cacheId, $result, _CACHE_TIME);
        }
        return $result;
    }

    public function defineSEODefault($id = 1) {
        $row = $this->getSEODefault($id);
        if ($row) {
            $res = array();
            $res['MetaID'] = $row['ID'];
            $res['MetaTitle'] = $row['Title'];
            $res['MetaKeywords'] = $row['Keywords'];
            $res['MetaDescription'] = $row['Description'];
            $res['MetaPicture'] = $row['Thumb'];
            return $res;
        }
    }

}
