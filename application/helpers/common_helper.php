<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Make an Url for link
 * @param	string
 * @return	string
 */
function href_link($page = '', $parameters = array()) {
    if (!empty($page)) {
        $link = ROOT_URL . '/';
        $link .= get_instance()->security->xss_clean($page);
        if (!stristr($page, '.html')) {
            $link .= '/';
        }
        $link = strtr($link, array('index.php' => '', '.php' => '.html'));
        return $link;
    } else {
        return ROOT_URL;
    }
}

/**
 * Make an Url for Upload file
 * @param	string
 * @return	string
 */
function href_upload_file($file = '', $parameters = array()) {
    if (!empty($file)) {
        $file = get_instance()->security->xss_clean($file);
        $ext_array = explode('.', $file);
        $ext = strtolower(end($ext_array));
        $allowed_ext = array('jpg', 'jpeg', 'png', 'gif', 'pdf');

        if (in_array($ext, $allowed_ext)) {
            $link = UPLOAD_URL . '/';
            $link .= $file;
            return $link;
        } else {
            return '';
        }
    }
    return '';
}

function remove_accents($string) {
    $chars = array(
        'à' => 'a', 'á' => 'a', 'ạ' => 'a', 'ả' => 'a', 'ã' => 'a', 'â' => 'a', 'ă' => 'a', 'ấ' => 'a', 'ầ' => 'a', 'ậ' => 'a', 'ẩ' => 'a', 'ẫ' => 'a', 'ă' => 'a', 'ằ' => 'a', 'ắ' => 'a', 'ặ' => 'a', 'ẳ' => 'a', 'ẵ' => 'a',
        'è' => 'e', 'é' => 'e', 'ẹ' => 'e', 'ẻ' => 'e', 'ẽ' => 'e', 'ê' => 'e', 'ề' => 'e', 'ế' => 'e', 'ệ' => 'e', 'ể' => 'e', 'ễ' => 'e',
        'ì' => 'i', 'í' => 'i', 'ị' => 'i', 'ỉ' => 'i', 'ĩ' => 'i',
        'ò' => 'o', 'ó' => 'o', 'ọ' => 'o', 'ỏ' => 'o', 'õ' => 'o', 'ô' => 'o', 'ồ' => 'o', 'ố' => 'o', 'ộ' => 'o', 'ổ' => 'o', 'ỗ' => 'o', 'ơ' => 'o', 'ờ' => 'o', 'ớ' => 'o', 'ợ' => 'o', 'ở' => 'o', 'ỡ' => 'o',
        'ù' => 'u', 'ú' => 'u', 'ụ' => 'u', 'ủ' => 'u', 'ũ' => 'u', 'ư' => 'u', 'ừ' => 'u', 'ứ' => 'u', 'ự' => 'u', 'ử' => 'u', 'ữ' => 'u',
        'ỳ' => 'y', 'ý' => 'y', 'ỵ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y',
        'đ' => 'd',
        'À' => 'A', 'Á' => 'A', 'Ạ' => 'A', 'Ả' => 'A', 'Ã' => 'A', 'Â' => 'A', 'Ầ' => 'A', 'Ấ' => 'A', 'Ầ' => 'A', 'Ậ' => 'A', 'Ẩ' => 'A', 'Ẫ' => 'A', 'Ă' => 'A', 'Ằ' => 'A', 'Ắ' => 'A', 'Ặ' => 'A', 'Ẳ' => 'A', 'Ẵ' => 'A',
        'È' => 'E', 'É' => 'E', 'Ẹ' => 'E', 'Ẻ' => 'E', 'Ẽ' => 'E', 'Ê' => 'E', 'Ề' => 'E', 'Ế' => 'E', 'Ệ' => 'E', 'Ể' => 'E', 'Ễ' => 'E',
        'Ì' => 'I', 'Í' => 'I', 'Ị' => 'I', 'Ỉ' => 'I', 'Ĩ' => 'I',
        'Ò' => 'O', 'Ó' => 'O', 'Ọ' => 'O', 'Ỏ' => 'O', 'Õ' => 'O', 'Ô' => 'O', 'Ồ' => 'O', 'Ố' => 'O', 'Ộ' => 'O', 'Ổ' => 'O', 'Ỗ' => 'O', 'Ơ' => 'O', 'Ờ' => 'O', 'Ớ' => 'O', 'Ợ' => 'O', 'Ở' => 'O', 'Ỡ' => 'O',
        'Ù' => 'U', 'Ú' => 'U', 'Ụ' => 'U', 'Ủ' => 'U', 'Ũ' => 'U', 'Ư' => 'U', 'Ừ' => 'U', 'Ứ' => 'U', 'Ự' => 'U', 'Ử' => 'U', 'Ữ' => 'U',
        'Ỳ' => 'Y', 'Ý' => 'Y', 'Ỵ' => 'Y', 'Ỷ' => 'Y', 'Ỹ' => 'Y',
        'Đ' => 'D',
    );
    $string = strtr($string, $chars);
    return $string;
}

function sanitize_title($title) {
    $title = remove_accents($title);
    return $title;
}

function to_slug($string) {
    $string = trim(sanitize_title($string));
    $string = rawurldecode($string);
    $string = preg_replace("/[^a-zA-Z0-9 -]/", "", $string);
    $string = strtolower($string);
    $string = str_replace(" ", '-', $string);
    $string = str_replace("--", '-', $string);
    return $string;
}

function to_tel($phone) {
    $phone = remove_accents($phone);
    $phone = preg_replace("/[^0-9]/", "", $phone);
    $phone = str_replace(" ", '', $phone);
    return $phone;
}

function get_current_url() {
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    return urlencode($url);
}

function format_price($number, $currency = 'đ', $format = 'vietnamese') {
    if (is_numeric($number)) {
        return number_format($number, 0, ",", ".") . $currency;
    } else {
        return false;
    }
}

//set cookie 1 month
function set_cookie_user($name, $value = '', $expire = '2592000', $domain = '', $path = '/', $prefix = '', $secure = NULL, $httponly = NULL) {
    set_cookie($name, $value, $expire);
}

function is_phone($phone) {
    $phone = to_tel($phone);
    $first_numberphone = substr($phone, 0, 1);
    $phonelen = strlen($phone);
    if ($first_numberphone == '0' && $phonelen > 9 && $phonelen < 12) {
        return true;
    } else {
        return false;
    }
}

/*
  return format phone for database
 */

function to_phone($phone) {
    return to_tel($phone);
}

function make_invoice_id($order_id, $phone) {
    $last4 = substr($phone, strlen($phone) - 4);
    return strtoupper(dechex($order_id)) . $last4;
}

function format_date($date) {
    $lang = get_instance()->config->item('language');
    if (isset($lang) && $lang == 'vietnamese') {
        return date('d-m-Y', strtotime($date));
    } else {
        return date('F j, Y', strtotime($date));
    }
}

?>