﻿// JavaScript Documentar 
ihpl_autosuggest = function (b, a, c, d) {
    this.textbox = b;
    this.url = c;
    this.text = b.value;
    this.parentSelector = d + " ";
    this.cur = -1;
    this.layer = null;
    this.flag = false;
    this.traversal = true;
    this.disabledTextBox = a;
    this.topSuggestion = null;
    this.currSuggestion = null;
    this.triggerSearch = true;
    this.listeners = {};
    this.init()
};
ihpl_autosuggest.prototype.init = function () {
    var a = this;
    this.textbox.onkeyup = function (b) {
        if (!b) {
            b = window.event
        }
        a.handleKeyUp(b)
    };
    this.textbox.onkeydown = function (b) {
        if (!b) {
            b = window.event
        }
        a.handleKeyDown(b)
    };
    this.textbox.onblur = function () {
        a.hideSuggestions();
        a.disabledTextBox.value = ""
    };
    this.textbox.onfocus = function (b) {
        if (!b) {
            b = window.event
        }
        a.handleKeyUp(b)
    }
};
ihpl_autosuggest.prototype.handleKeyUp = function (c) {
    var f = c.keyCode;
    var b = this;
    var a = this.textbox.value;
    var d = a.replace(/(^\s+|\s+$)/g, "");
    d = d.replace("/", " ", "g");
    if (d.length) {
        var e = /^[a-z]+$/;
        this.flag = e.test(a)
    }
    clearTimeout(this.timeoutId);
    if (f == 8 || f == 46) {
        this.disabledTextBox.value = "";
        this.hideSuggestions();
        this.traversal = false;
        this.cur = -1;
        $(this.textbox)
            .attr("entityType", "");
        $(this.textbox)
            .attr("entityId", "");
        $(this.textbox)
            .attr("cityName", "");
        $(this.textbox)
            .attr("cityId", "");
        this.timeoutId = setTimeout(function () {
            b.requestSuggestions(b, d)
        }, 250)
    }
    else {
        if ((f < 16 || (f >= 17 && f < 32)) || (f >= 33 && f <= 46) || (f >= 112 && f <= 123)) {}
        else {
            this.timeoutId = setTimeout(function () {
                b.requestSuggestions(b, d)
            }, 250)
        }
    }
};
ihpl_autosuggest.prototype.requestSuggestions = function (c, a) {
    var b = this;
    if (a.length > 1) {
        if (ihpl.hotels.currentView && (ihpl.hotels.currentView != "home")) {
            var d = 'params={"method" : "hotel_json.php","search_query" :"' + a + '","city_id" :"' + ihpl.hotels.cityId + '","limit":10}'
        }
        else {
            var d = 'params={"method" : "hotel_json.php","search_query" :"' + a + '","limit":10}'
        }
		
        $.ajaxSetup({
            cache: true
        });
        $.ajax({
            url: c.url,
            type: "GET",
            data: d,
            dataType: "jsonp",
            jsonpCallback: "myCallback",
            success: function (e) { 
                if (e.data.r.length) {
                    b.createSuggestionsList(e)
                }
            }
        })
    }
    else
    {
        document.querySelector(this.parentSelector + " .search_res_list").innerHTML = "";
    }
};
ihpl_autosuggest.prototype.createSuggestionsList = function (a) {
	
    var c = document.querySelector(this.parentSelector + " .search_res_list");
    var l = document.createDocumentFragment();
    c.innerHTML = "";
	cd=document.querySelector(this.parentSelector + " .ht_asuggest_n")
	if(cd!=null){
		cd.style.margin="2px";
		cd.style.marginTop="0px";
	}
	var u = this;
    this.layer = c;
    for (var x = 0; x < a.data.r.length; x++) {
        var p = a.data.r[x].n;
        var o = a.data.r[x].ct._id;
        var h = a.data.r[x].ct.n;
        var m = a.data.r[x]._id;
        if (x == 0) {
            var f = p
        }
        var k = "(" + a.data.r[x].nh + " hotels)";
        var v = a.data.r[x].t;
        if (v == "hotel") {
            k = ""
        }
        var g = "ht_bgAutoSuggest ht_" + v;
        var d = document.createElement("li");
        var s = document.createElement("a");
        var q = document.createDocumentFragment();
        var e = document.createElement("strong");
        var r = p;
        if (p.length > 49) {
            r = r.substr(0, 49) + "..."
        }
        e.innerHTML = r;
        e.setAttribute("title", p);
        e.setAttribute("cityId", o);
        e.setAttribute("cityName", h);
        e.setAttribute("entityId", m);
        e.setAttribute("entityType", v);
        var y = document.createElement("span");
        y.innerHTML = k;
        q.appendChild(e);
        q.appendChild(y);
        var j = document.createElement("em");
        var b = document.createElement("small");
        b.innerHTML = v;
        j.appendChild(b);
        var w = document.createElement("i");
        w.className = g;
        j.appendChild(w);
        q.appendChild(j);
        s.appendChild(q);
        d.appendChild(s);
        l.appendChild(d)
    }
    c.appendChild(l);
    this.traversal = true;
    this.showSuggestions(this.layer);
    this.currSuggestion = document.querySelector(this.parentSelector + ".search_res_list li");
    this.topSuggestion = document.querySelector(this.parentSelector + ".search_res_list li strong")
        .innerHTML.toLowerCase();
    this.layer.onmousedown = this.layer.onmouseup = this.layer.onmouseover = function (z) {
        z = z || window.event;
        oTarget = z.target || z.srcElement;
        if (z.type == "mousedown") {
            if (oTarget) {
                u.selectSuggestion(oTarget);
                u.hideSuggestions()
            }
        }
        else {
            if (z.type == "mouseover") {
                u.highlightSuggestion(oTarget)
            }
            else {
                u.textbox.focus()
            }
        }
    }
};
ihpl_autosuggest.prototype.selectSuggestion = function (d) {
    var c = d.tagName.toUpperCase();
    var b = "";
    var a = "";
    var e = "";
    this.cur = -1;
    switch (c) {
    case "A":
        b = $(d)
            .children("strong")
            .html();
        a = $(d)
            .children("strong")
            .attr("cityId");
        c_name = $(d)
            .children("strong")
            .attr("cityName");
        e = $(d)
            .children("strong")
            .attr("entityId");
        e_type = $(d)
            .children("strong")
            .attr("entityType");
        break;
    case "STRONG":
        b = $(d)
            .html();
        a = $(d)
            .attr("cityId");
        c_name = $(d)
            .attr("cityName");
        e = $(d)
            .attr("entityId");
        e_type = $(d)
            .attr("entityType");
        break;
    case "LI":
        b = $(d)
            .find("strong")
            .html();
        a = $(d)
            .find("strong")
            .attr("cityId");
        c_name = $(d)
            .find("strong")
            .attr("cityName");
        e = $(d)
            .find("strong")
            .attr("entityId");
        e_type = $(d)
            .find("strong")
            .attr("entityType");
        break;
    case "SMALL":
    case "I":
        b = $(d)
            .parent()
            .siblings("strong")
            .html();
        a = $(d)
            .parent()
            .siblings("strong")
            .attr("cityId");
        c_name = $(d)
            .parent()
            .siblings("strong")
            .attr("cityName");
        e = $(d)
            .parent()
            .siblings("strong")
            .attr("entityId");
        e_type = $(d)
            .parent()
            .siblings("strong")
            .attr("entityType");
        break;
    default:
        b = $(d)
            .siblings("strong")
            .html();
        a = $(d)
            .siblings("strong")
            .attr("cityId");
        c_name = $(d)
            .siblings("strong")
            .attr("cityName");
        e = $(d)
            .siblings("strong")
            .attr("entityId");
        e_type = $(d)
            .siblings("strong")
            .attr("entityType")
    }
    this.textbox.value = b.toLowerCase();
	//$("#auto-suggest-option").val(e+"-"+a);
	
	$("#gi_search").attr("action",a+".html");
		
    /*this.textbox.setAttribute("cityId", a);
    this.textbox.setAttribute("cityName", c_name);
    this.textbox.setAttribute("entityId", e);
    this.textbox.setAttribute("entityType", e_type);*/
    this.disabledTextBox.value = "";
    this.hideSuggestions();
    if (this.triggerSearch) {
        this.trigger("selected")
    }
};
ihpl_autosuggest.prototype.addListener = function (a, b) {
    this.listeners[a] = b
};
ihpl_autosuggest.prototype.trigger = function (a) {
    if (this.listeners[a]) {
        this.listeners[a]()
    }
};
ihpl_autosuggest.prototype.showSuggestions = function (a) {
    $(".ht_asuggest.dn")
        .removeClass("dn")
};
ihpl_autosuggest.prototype.hideSuggestions = function () {
    $(".ht_asuggest")
        .addClass("dn")
};
ihpl_autosuggest.prototype.goToSuggestion = function (a) {
    var b = $(this.layer)
        .children("li");
    if (b.length) {
        if (a > 0) {
            if (this.cur < b.length - 1) {
                this.currSuggestion = b[++this.cur]
            }
        }
        else {
            if (this.cur > 0) {
                this.currSuggestion = b[--this.cur]
            }
        } if (this.currSuggestion) {
            this.highlightSuggestion(this.currSuggestion);
            this.textbox.value = $(this.currSuggestion)
                .find("strong")
                .html()
        }
    }
};
ihpl_autosuggest.prototype.highlightSuggestion = function (b) {
    for (var a = 0; a < this.layer.childNodes.length; a++) {
        var c = this.layer.childNodes[a];
        if (c == b) {
            c.firstChild.className = "current"
        }
        else {
            if (c.firstChild.className == "current") {
                c.firstChild.className = ""
            }
        }
    }
};
ihpl_autosuggest.prototype.handleKeyDown = function (a) {
    switch (a.keyCode) {
    case 38:
        if (this.traversal) {
            this.goToSuggestion(-1)
        }
        break;
    case 40:
        if (this.traversal) {
            this.goToSuggestion(1)
        }
        break;
    case 27:
        this.textbox.value = this.text;
        this.disabledTextBox.value = "";
        this.hideSuggestions();
        this.traversal = false;
        this.currSuggestion = null;
        break;
    case 9:
        this.triggerSearch = false;
        if (this.currSuggestion) {
            this.selectSuggestion(this.currSuggestion)
        }
        this.hideSuggestions();
        this.traversal = false;
        a.returnValue = false;
        if (a.preventDefault) {
            a.preventDefault()
        }
        break;
    case 13:
        this.triggerSearch = true;
        if (this.currSuggestion) {
            this.selectSuggestion(this.currSuggestion)
        }
        this.hideSuggestions();
        this.traversal = false;
        a.returnValue = false;
        if (a.preventDefault) {
            a.preventDefault()
        }
        break
    }
};
//var proto;
var api_url =  "/js/hotel_json.php";
//var api_url = "http://server/IHPL/js/hotel_json.php";
var as = new ihpl_autosuggest(document.querySelector("#gi_destination_city"), document.querySelector("#auto-suggest-input"), api_url, "#homeSearch");
as.addListener("selected", function () {
    $("#hm_srch").trigger("click");
});
var ihpl = ihpl || {};
ihpl.hotels = {
    cityName: "",
    cityId: null,
    cityIdPrev: null,
    cityNamePrev: "",
    hotelCode: null,
    hotelName: "",
    entityId: null,
    entityType: "",
    entityName: "",
    toDetail: "",
    currentView: "",
    checkin: new Date(new Date()
        .getTime() + 7 * 24 * 60 * 60 * 1000),
    checkout: new Date(new Date()
        .getTime() + 8 * 24 * 60 * 60 * 1000),
    selected: false,
    rooms: 1,
    roomString: "1-2_0",
    queryString: "",
    controllerSearchString: "",
    hotelDetailLink: "",
    staticRetry: false,
    controllerRetry: false,
    searchButtonClick: false,
    widgetUsed: false,
    isCycloneFailed: false,
    fwdp: {},
    getSearchParams: function () {
        return this.controllerSearchString.split("-")
    },
   
  
};


